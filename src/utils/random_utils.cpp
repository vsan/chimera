#include "../resources/shaders/crandom.h"

std::vector<uint32_t> GenerateInitialRandState(int a_seed, size_t a_size)
{
  std::vector<uint32_t> res(a_size * 2);
  for (size_t i = 0; i < a_size; i++)
  {
    auto state = RandomGenInit(a_seed * i);
    res[i * 2 + 0] = state.x;
    res[i * 2 + 1] = state.y;
    NextState(state);
  }

  return res;
}
