#ifndef CHIMERA_RANDOM_UTILS_H
#define CHIMERA_RANDOM_UTILS_H

std::vector<uint32_t> GenerateInitialRandState(int a_seed, size_t a_size);

#endif//CHIMERA_RANDOM_UTILS_H
