#include <chrono>
#include "render/render_common.h"
#include "glfw_window.h"


int main()
{
  std::shared_ptr<IRender> app = CreateRender(1024, 1024, RenderEngineType::RTX_QUERY);
  auto* window = Init(app, 0);

  auto start = std::chrono::high_resolution_clock::now();
//  app->LoadScene("/media/vsan/Data/scenes/bistro_exterior/scenelib/statex_00001.xml", false);
  //  app->LoadScene("../resources/scenes/textured_primitives/statex_00001.xml", false);
    app->LoadScene("/home/vsan/repos/glTF-Sample-Models/2.0/Buggy/glTF/Buggy.gltf", false);
//    app->LoadScene("/home/vsan/repos/glTF-Sample-Models/2.0/Sponza/glTF/Sponza.gltf", false);
  //  app->LoadScene("../resources/scenes/WaterBottle/WaterBottle.gltf", false);
  auto end = std::chrono::high_resolution_clock::now();
  auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

  std::cout << "Load scene time: " << ms << " milliseconds" << std::endl;

  MainLoop(app, window);

  return 0;
}
