#ifndef VK_RENDER_H
#define VK_RENDER_H

#define VK_NO_PROTOTYPES
#include "scene_mgr.h"
#include <geom/vk_mesh.h>
#include <vk_descriptor_sets.h>
#include <vk_fbuf_attachment.h>
#include <vk_buffers.h>
#include <vk_images.h>
#include <vk_swapchain.h>
#include "render_common.h"
#include <string>
#include <iostream>
#include <array>

class RenderEngine : public IRender
{
public:
  RenderEngine(uint32_t a_width, uint32_t a_height);
  ~RenderEngine()  { Cleanup(); };

  inline uint32_t     GetWidth()      const override { return m_width; }
  inline uint32_t     GetHeight()     const override { return m_height; }
  inline VkInstance   GetVkInstance() const override { return m_instance; }
  void InitVulkan(std::vector<const char*> a_instanceExtensions, uint32_t a_deviceId) override;

  void InitPresentation(VkSurfaceKHR& a_surface) override;

  Camera GetCurrentCamera() override {return m_cam;}
  void ProcessInput(AppInput& input) override;
  void UpdateCamera(const Camera &cam) override;
  void UpdateView();

  void LoadScene(const std::string &path, bool transpose_inst_matrices) override;
//  void LoadTestTriangle();
  void DrawFrame() override;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // debugging utils
  //
  static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallbackFn(
    VkDebugReportFlagsEXT                       flags,
    VkDebugReportObjectTypeEXT                  objectType,
    uint64_t                                    object,
    size_t                                      location,
    int32_t                                     messageCode,
    const char* pLayerPrefix,
    const char* pMessage,
    void* pUserData)
  {
    std::cout << pLayerPrefix << " " << pMessage << std::endl;
    return VK_FALSE;
  }

  VkDebugReportCallbackEXT m_debugReportCallback = nullptr;

  static constexpr uint64_t scratchMemSize = 16 * 16 * 1024u; //@TODO: calculate it somehow?
private:

  bool m_sceneReload = false;
  std::string m_initialScenePath;

  struct RenderSettings
  {
    bool wireframe_mode = false;
    bool gbuf_mode = false;
  } m_renderSettings;

  VkInstance       m_instance       = VK_NULL_HANDLE;
  VkCommandPool    m_commandPool    = VK_NULL_HANDLE;
  VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
  VkDevice         m_device         = VK_NULL_HANDLE;
  VkQueue          m_graphicsQueue  = VK_NULL_HANDLE;
  VkQueue          m_transferQueue  = VK_NULL_HANDLE;
  VkQueue          m_computeQueue   = VK_NULL_HANDLE;

  vk_utils::QueueFID_T m_queueFamilyIDXs {UINT32_MAX, UINT32_MAX, UINT32_MAX};

  std::shared_ptr<vk_utils::ICopyEngine> m_pCopyHelper;

  struct
  {
    uint32_t    currentFrame      = 0u;
    VkQueue     queue             = VK_NULL_HANDLE;
    VkSemaphore imageAvailable    = VK_NULL_HANDLE;
    VkSemaphore renderingFinished = VK_NULL_HANDLE;
  } m_presentationResources;

  std::vector<VkFence> m_frameFences;
  std::vector<VkCommandBuffer> m_cmdBuffersDrawMain;
  std::vector<VkCommandBuffer> m_cmdBuffersDrawWire;
  std::vector<VkCommandBuffer> m_cmdBuffersDrawOffscreen;

  VkSemaphore m_offscreenSemaphore = VK_NULL_HANDLE;
//  PushConst2MatricesID_T pushConsts;
  PushConstMatrixViewID_T pushConsts;

  pipeline_data_t m_basicForwardPipeline;
  pipeline_data_t m_pbrForwardPipeline;
  pipeline_data_t m_wireframePipeline;

  VkDescriptorSet       m_quadDS = VK_NULL_HANDLE;
  VkDescriptorSetLayout m_quadDSLayout = nullptr;

  VkDescriptorSet       m_pbrForwardDS = VK_NULL_HANDLE;
  VkDescriptorSetLayout m_pbrForwardDSLayout = nullptr;

  pipeline_data_t m_gbufPipeline;
  pipeline_data_t m_quadPipeline;

  VkDescriptorSet       m_GBufDS = VK_NULL_HANDLE;
  VkDescriptorSetLayout m_GBufDSLayout = nullptr;

  std::unique_ptr<vk_utils::RenderTarget> m_GBufTarget;
  enum GBUF_ATTACHMENT
  {
    POS_Z = 1,
    NORMAL = 2,
    ALBEDO = 3,
    TOTAL = 4
  };
  std::array<uint32_t, GBUF_ATTACHMENT::TOTAL> m_GBuf_idx;
  VkDeviceMemory m_attachmentsMem = VK_NULL_HANDLE;
  VkRenderPass m_screenRenderPass = VK_NULL_HANDLE; // main renderpass

//  std::shared_ptr<vk_utils::ProgramBindings> m_pBindings = nullptr;
  std::shared_ptr<vk_utils::DescriptorMaker> m_pBindings = nullptr;

  VkSurfaceKHR m_surface;
  VulkanSwapChain m_swapchain;
  std::vector<VkFramebuffer> m_frameBuffers;

  // screen depthbuffer
  std::vector<VkFormat> m_depthFormats = {
    VK_FORMAT_D32_SFLOAT,
    VK_FORMAT_D32_SFLOAT_S8_UINT,
    VK_FORMAT_D24_UNORM_S8_UINT,
    VK_FORMAT_D16_UNORM_S8_UINT,
    VK_FORMAT_D16_UNORM
  };
  vk_utils::VulkanImageMem m_depthBuffer;

  Camera   m_cam;
  uint32_t m_width  = 1024u;
  uint32_t m_height = 1024u;
  uint32_t m_framesInFlight = 2u;
  bool m_vsync = false;

  VkPhysicalDeviceFeatures m_enabledDeviceFeatures = {};
  std::vector<const char*> m_deviceExtensions      = {};
  std::vector<const char*> m_instanceExtensions    = {};

  bool m_enableValidation;
  std::vector<const char*> m_validationLayers;

  std::shared_ptr<SceneManager> m_pScnMgr;

  void DrawFrameDeferred();
  void DrawFrameSimple();

  void CreateInstance();
  void CreateDevice(uint32_t a_deviceId);

  void BuildCommandBufferForward(VkCommandBuffer a_cmdBuff, VkFramebuffer a_frameBuff, pipeline_data_t a_pipeline, VkDescriptorSet dSet);
  void BuildCommandBufferQuad(VkCommandBuffer cmdBuff, VkFramebuffer frameBuff);
  void BuildCommandBufferOffscreen(VkCommandBuffer cmdBuff, uint32_t a_fbufIdx);

  void SetupOffscreenRendering();
  void SetupQuadPipeline();
  void SetupSimplePipeline();
  void SetupPbrForwardPipeline();
  void CleanupPipelineAndSwapchain();
  void RecreateSwapChain();

  void Cleanup();

  void SetupDeviceFeatures();
  void SetupDeviceExtensions();
  void SetupValidationLayers();

};


#endif//VK_RENDER_H
