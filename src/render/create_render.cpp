#include "render_common.h"
#include "deferred_render.h"
#include "basic_rt_render.h"
#include "pt_render.h"
#include "rt_rayquery_render.h"
#include "hy_render.h"


std::unique_ptr<IRender> CreateRender(uint32_t w, uint32_t h, RenderEngineType type)
{
  switch(type)
  {
  case RenderEngineType::DEFERRED:
    return std::make_unique<RenderEngine>(w, h);
  case RenderEngineType::RTX_PIPELINE_BASIC:
    return std::make_unique<RenderEngineBasicRT>(w, h);
  case RenderEngineType::RTX_PIPELINE_PT:
    return std::make_unique<RenderEnginePT>(w, h);
  case RenderEngineType::RTX_QUERY:
    return std::make_unique<RenderEngineRayQuery>(w, h);
  case RenderEngineType::FORWARD_PLUS_RAYQUERY:
    return std::make_unique<RenderEngineHybrid>(w, h);
  }

  return nullptr;
}


