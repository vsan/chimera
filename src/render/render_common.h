#ifndef CHIMERA_RENDER_COMMON_H
#define CHIMERA_RENDER_COMMON_H

#include "volk.h"
#include "vk_utils.h"
#include "../Camera.h"
#include <cstring>
#include <memory>

struct AppInput
{
  enum {MAXKEYS = 384};
  Camera cam;
  bool   keyPressed[MAXKEYS]{};
  bool   keyReleased[MAXKEYS]{};
  bool   cameraInternalUpdate = false;
  void clearKeys() { memset(keyPressed, 0, MAXKEYS*sizeof(bool)); memset(keyReleased, 0, MAXKEYS*sizeof(bool)); }
};

struct pipeline_data_t
{
  VkPipelineLayout layout = VK_NULL_HANDLE;
  VkPipeline pipeline     = VK_NULL_HANDLE;
};

class IRender
{
public:
  virtual uint32_t     GetWidth() const = 0;
  virtual uint32_t     GetHeight() const = 0;
  virtual VkInstance   GetVkInstance() const = 0;

  virtual void InitVulkan(std::vector<const char*> a_instanceExtensions, uint32_t a_deviceId) = 0;
  virtual void InitPresentation(VkSurfaceKHR& a_surface) = 0;

  virtual void ProcessInput(AppInput& input) = 0;
  virtual void UpdateCamera(const Camera &cam) = 0;
  virtual Camera GetCurrentCamera() = 0;

  virtual void LoadScene(const std::string &path, bool transpose_inst_matrices) = 0;
  virtual void DrawFrame() = 0;
};

enum class RenderEngineType
{
  DEFERRED,
  RTX_PIPELINE_BASIC,
  RTX_PIPELINE_PT,
  RTX_QUERY,
  FORWARD_PLUS_RAYQUERY
};

static const char* DEFAULT_SCENE_PATH = "../resources/scenes/textured_primitives/statex_00001.xml";
std::unique_ptr<IRender> CreateRender(uint32_t w, uint32_t h, RenderEngineType type);

#endif//CHIMERA_RENDER_COMMON_H
