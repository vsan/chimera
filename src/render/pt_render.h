#ifndef CHIMERA_PT_RENDER_H
#define CHIMERA_PT_RENDER_H

#define VK_NO_PROTOTYPES
#include "scene_mgr.h"
#include <geom/vk_mesh.h>
#include <vk_descriptor_sets.h>
#include <vk_fbuf_attachment.h>
#include "../resources/shaders/common.h"
#include <vk_images.h>
#include <vk_buffers.h>
#include <vk_swapchain.h>
#include <vk_copy.h>
#include "render_common.h"
#include <iostream>


class RenderEnginePT : public IRender
{
public:
  RenderEnginePT(uint32_t a_width, uint32_t a_height);
  ~RenderEnginePT()  { Cleanup(); };

  inline uint32_t     GetWidth()      const override { return m_width; }
  inline uint32_t     GetHeight()     const override { return m_height; }
  inline VkInstance   GetVkInstance() const override { return m_instance; }
  void InitVulkan(std::vector<const char*> a_instanceExtensions, uint32_t a_deviceId) override;

  void InitPresentation(VkSurfaceKHR& a_surface) override;

  void ProcessInput(AppInput& input) override {};
  void UpdateCamera(const Camera &cam) override;
  Camera GetCurrentCamera() override {return m_cam;}

  void LoadScene(const std::string &path, bool transpose_inst_matrices) override;
//  void LoadTestTriangle();
  void DrawFrame() override;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // debugging utils
  //
  static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallbackFn(
    VkDebugReportFlagsEXT                       flags,
    VkDebugReportObjectTypeEXT                  objectType,
    uint64_t                                    object,
    size_t                                      location,
    int32_t                                     messageCode,
    const char* pLayerPrefix,
    const char* pMessage,
    void* pUserData)
  {
    std::cout << pLayerPrefix << " " << pMessage << std::endl;
    return VK_FALSE;
  }

  VkDebugReportCallbackEXT m_debugReportCallback = nullptr;

  static constexpr uint64_t scratchMemSize = 16 * 1024u * 1024u;
private:

  void * m_pDeviceFeatures;

  struct RenderSettings
  {

  } m_renderSettings;

  VkInstance       m_instance       = VK_NULL_HANDLE;
  VkCommandPool    m_commandPool    = VK_NULL_HANDLE;
  VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
  VkDevice         m_device         = VK_NULL_HANDLE;
  VkQueue          m_graphicsQueue  = VK_NULL_HANDLE;
  VkQueue          m_transferQueue  = VK_NULL_HANDLE;
  VkQueue          m_computeQueue   = VK_NULL_HANDLE;

  vk_utils::QueueFID_T m_queueFamilyIDXs {UINT32_MAX, UINT32_MAX, UINT32_MAX};

  VkPhysicalDeviceRayTracingPipelinePropertiesKHR  m_rtPipelineProperties{};
  VkPhysicalDeviceRayTracingPipelineFeaturesKHR m_enabledRTPipelineFeatures{};
  VkPhysicalDeviceAccelerationStructureFeaturesKHR m_accelStructFeatures{};
  VkPhysicalDeviceAccelerationStructureFeaturesKHR m_enabledAccelStructFeatures{};
  VkPhysicalDeviceBufferDeviceAddressFeatures m_enabledDeviceAddressFeatures{};

  VkDeviceSize m_scratchMemSize = 0u;
  std::shared_ptr<vk_utils::ICopyEngine> m_pCopyHelper;

  struct
  {
    uint32_t    currentFrame      = 0u;
    VkQueue     queue             = VK_NULL_HANDLE;
    VkSemaphore imageAvailable    = VK_NULL_HANDLE;
    VkSemaphore renderingFinished = VK_NULL_HANDLE;
  } m_presentationResources;

  std::vector<VkFence> m_frameFences;
  std::vector<VkCommandBuffer> m_cmdBuffersDrawMain;

  struct
  {
    LiteMath::float4x4 projView;
    LiteMath::float4x4 model;
  } pushConst2M;

  pipeline_data_t m_rtPipeline;
  uint32_t m_numShaderGroups;

  VkBuffer m_SBT = VK_NULL_HANDLE;
  VkDeviceMemory m_SBTAlloc = VK_NULL_HANDLE;
  using Stride = VkStridedDeviceAddressRegionKHR;
  std::vector<Stride> m_SBTStrides;

  VkDescriptorSet m_rtDS = VK_NULL_HANDLE;
  VkDescriptorSetLayout m_rtDSLayout = VK_NULL_HANDLE;

  std::shared_ptr<vk_utils::DescriptorMaker> m_pBindings = nullptr;

  VkSurfaceKHR m_surface = VK_NULL_HANDLE;
  VulkanSwapChain m_swapchain;

  SwapchainAttachment m_storageImage;
  SwapchainAttachment m_rndStateImage;

  VkDeviceMemory m_imageMemory = VK_NULL_HANDLE;


  UniformParams m_uniforms;
  VkBuffer m_ubo = VK_NULL_HANDLE;
  VkDeviceMemory m_uboAlloc = VK_NULL_HANDLE;
  void *m_uboMappedMem = nullptr;

  Camera   m_cam;
  uint32_t m_width  = 1024u;
  uint32_t m_height = 1024u;
  uint32_t m_framesInFlight = 2u;
  bool m_vsync = false;

  VkPhysicalDeviceFeatures m_enabledDeviceFeatures = {};
  std::vector<const char*> m_deviceExtensions      = {};
  std::vector<const char*> m_instanceExtensions    = {};

  bool m_enableValidation;
  std::vector<const char*> m_validationLayers;

  std::shared_ptr<SceneManager> m_pScnMgr;

  void DrawFrameRT();

  void CreateInstance();
  void CreateDevice(uint32_t a_deviceId);

  void BuildCommandBufferRT(VkCommandBuffer cmdBuff, uint32_t swapChainIDX);

  void SetupRTPipeline();
  void CreateShaderBindingTables();
  void CreateUniformBuffer();
  void CreateStorageImage();

  void UpdateUniformBuffer();

  void CleanupPipelineAndSwapchain();
  void RecreateSwapChain();

  void Cleanup() {}; //@TODO

  void GetRTFeatures();
  void SetupDeviceFeatures();
  void SetupDeviceExtensions();
  void SetupValidationLayers();


};

#endif//CHIMERA_PT_RENDER_H
