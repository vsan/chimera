#ifndef CHIMERA_RQ_RENDER_H
#define CHIMERA_RQ_RENDER_H

#define VK_NO_PROTOTYPES
#include "scene_mgr.h"
#include "../resources/shaders/common.h"
#include <geom/vk_mesh.h>
#include <vk_descriptor_sets.h>
#include <vk_fbuf_attachment.h>
#include <vk_copy.h>
#include <vk_images.h>
#include <vk_buffers.h>
#include <vk_swapchain.h>
#include "render_common.h"
#include <string>
#include <iostream>


class RenderEngineRayQuery : public IRender
{
public:
  RenderEngineRayQuery(uint32_t a_width, uint32_t a_height);
  ~RenderEngineRayQuery()  { Cleanup(); };

  inline uint32_t     GetWidth()      const override { return m_width; }
  inline uint32_t     GetHeight()     const override { return m_height; }
  inline VkInstance   GetVkInstance() const override { return m_instance; }
  void InitVulkan(std::vector<const char*> a_instanceExtensions, uint32_t a_deviceId) override;

  void InitPresentation(VkSurfaceKHR& a_surface) override;

  void ProcessInput(AppInput& input) override {};
  void UpdateCamera(const Camera &cam) override;
  Camera GetCurrentCamera() override {return m_cam;}

  void LoadScene(const std::string &path, bool transpose_inst_matrices) override;
//  void LoadTestTriangle();
  void DrawFrame() override;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // debugging utils
  //
  static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallbackFn(
    VkDebugReportFlagsEXT                       flags,
    VkDebugReportObjectTypeEXT                  objectType,
    uint64_t                                    object,
    size_t                                      location,
    int32_t                                     messageCode,
    const char* pLayerPrefix,
    const char* pMessage,
    void* pUserData)
  {
    std::cout << pLayerPrefix << " " << pMessage << std::endl;
    return VK_FALSE;
  }

  VkDebugReportCallbackEXT m_debugReportCallback = nullptr;

  static constexpr uint64_t scratchMemSize = 16 * 16 * 1024u;
  static constexpr uint32_t wgWidth  = 16;
  static constexpr uint32_t wgHeight = 16;
private:

  void * m_pDeviceFeatures;

  VkInstance       m_instance       = VK_NULL_HANDLE;
  VkCommandPool    m_commandPool    = VK_NULL_HANDLE;
  VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
  VkDevice         m_device         = VK_NULL_HANDLE;
  VkQueue          m_graphicsQueue  = VK_NULL_HANDLE;
  VkQueue          m_transferQueue  = VK_NULL_HANDLE;
  VkQueue          m_computeQueue   = VK_NULL_HANDLE;

  vk_utils::QueueFID_T m_queueFamilyIDXs {UINT32_MAX, UINT32_MAX, UINT32_MAX};

  VkPhysicalDeviceAccelerationStructureFeaturesKHR m_accelStructFeatures{};
  VkPhysicalDeviceAccelerationStructureFeaturesKHR m_enabledAccelStructFeatures{};
  VkPhysicalDeviceBufferDeviceAddressFeatures m_enabledDeviceAddressFeatures{};
  VkPhysicalDeviceRayQueryFeaturesKHR m_enabledRayQueryFeatures;

  std::shared_ptr<vk_utils::ICopyEngine> m_pCopyHelper;

  struct
  {
    uint32_t    currentFrame      = 0u;
    VkQueue     queue             = VK_NULL_HANDLE;
    VkSemaphore imageAvailable    = VK_NULL_HANDLE;
    VkSemaphore renderingFinished = VK_NULL_HANDLE;
  } m_presentationResources;

  std::vector<VkFence> m_frameFences;
  std::vector<VkCommandBuffer> m_cmdBuffersDrawMain;

  struct
  {
    LiteMath::float4x4 projView;
    LiteMath::float4x4 model;
  } pushConst2M;

  pipeline_data_t m_rtPipeline;

  VkDescriptorSet       m_rtDS;
  VkDescriptorSetLayout m_rtDSLayout = nullptr;

  std::shared_ptr<vk_utils::DescriptorMaker> m_pBindings = nullptr;

  VkSurfaceKHR m_surface;
  VulkanSwapChain m_swapchain;

  std::vector<VkFormat> m_depthFormats = {
    VK_FORMAT_D32_SFLOAT,
    VK_FORMAT_D32_SFLOAT_S8_UINT,
    VK_FORMAT_D24_UNORM_S8_UINT,
    VK_FORMAT_D16_UNORM_S8_UINT,
    VK_FORMAT_D16_UNORM
  };
  vk_utils::VulkanImageMem m_depthBuffer;

  struct
  {
    SwapchainAttachment attachment {};
    VkDeviceMemory memory = VK_NULL_HANDLE;
  } m_storageImage;

  UniformParams m_uniforms;
  VkBuffer m_ubo;
  VkDeviceMemory m_uboAlloc;
  void* m_uboMappedMem;

  Camera   m_cam;
  uint32_t m_width  = 1024u;
  uint32_t m_height = 1024u;
  uint32_t m_framesInFlight = 2u;
  bool m_vsync = false;

  VkPhysicalDeviceFeatures m_enabledDeviceFeatures = {};
  std::vector<const char*> m_deviceExtensions      = {};
  std::vector<const char*> m_instanceExtensions    = {};

  bool m_enableValidation;
  std::vector<const char*> m_validationLayers;

  std::shared_ptr<SceneManager> m_pScnMgr;

  void DrawFrameRT();

  void CreateInstance();
  void CreateDevice(uint32_t a_deviceId);

  void BuildCommandBufferRT(VkCommandBuffer cmdBuff, uint32_t swapChainIDX);

  void SetupRTPipeline();
  void CreateUniformBuffer();
  void CreateStorageImage();

  void UpdateUniformBuffer();

  void CleanupPipelineAndSwapchain();
  void RecreateSwapChain();

  void Cleanup() {}; //@TODO

  void GetRTFeatures();
  void SetupDeviceFeatures();
  void SetupDeviceExtensions();
  void SetupValidationLayers();


};

#endif//CHIMERA_RQ_RENDER_H
