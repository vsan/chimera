#include "rt_rayquery_render.h"
#include <ray_tracing/vk_rt_funcs.h>
#include <vk_pipeline.h>

RenderEngineRayQuery::RenderEngineRayQuery(uint32_t a_width, uint32_t a_height) :  m_width(a_width), m_height(a_height)
{
#ifdef NDEBUG
  m_enableValidation = false;
#else
  m_enableValidation = true;
#endif

}

void RenderEngineRayQuery::SetupDeviceFeatures()
{
  //m_enabledDeviceFeatures

  m_enabledRayQueryFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
  m_enabledRayQueryFeatures.rayQuery = VK_TRUE;

  m_enabledDeviceAddressFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
  m_enabledDeviceAddressFeatures.bufferDeviceAddress = VK_TRUE;
  m_enabledDeviceAddressFeatures.pNext = &m_enabledRayQueryFeatures;

  m_enabledAccelStructFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
  m_enabledAccelStructFeatures.accelerationStructure = VK_TRUE;
  m_enabledAccelStructFeatures.pNext = &m_enabledDeviceAddressFeatures;

  m_pDeviceFeatures = &m_enabledAccelStructFeatures;
}

void RenderEngineRayQuery::SetupDeviceExtensions()
{
  m_deviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

  m_deviceExtensions.push_back(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
  m_deviceExtensions.push_back(VK_KHR_RAY_QUERY_EXTENSION_NAME);

  // Required by VK_KHR_acceleration_structure
  m_deviceExtensions.push_back(VK_KHR_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME);
  m_deviceExtensions.push_back(VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME);
  m_deviceExtensions.push_back(VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME);

  // Required by VK_KHR_ray_tracing_pipeline
  m_deviceExtensions.push_back(VK_KHR_SPIRV_1_4_EXTENSION_NAME);

  // Required by VK_KHR_spirv_1_4
  m_deviceExtensions.push_back(VK_KHR_SHADER_FLOAT_CONTROLS_EXTENSION_NAME);
}

void RenderEngineRayQuery::SetupValidationLayers()
{
  m_validationLayers.push_back("VK_LAYER_KHRONOS_validation");
  m_validationLayers.push_back("VK_LAYER_LUNARG_monitor");

  //useful layers: VK_LAYER_LUNARG_standard_validation, VK_LAYER_LUNARG_monitor, VK_LAYER_LUNARG_api_dump, VK_LAYER_KHRONOS_validation
}

void RenderEngineRayQuery::GetRTFeatures()
{
  m_accelStructFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;

  VkPhysicalDeviceFeatures2 deviceFeatures2{};
  deviceFeatures2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
  deviceFeatures2.pNext = &m_accelStructFeatures;
  vkGetPhysicalDeviceFeatures2(m_physicalDevice, &deviceFeatures2);
}

void RenderEngineRayQuery::InitVulkan(std::vector<const char*> a_instanceExtensions, uint32_t a_deviceId)
{
  m_instanceExtensions = std::move(a_instanceExtensions);
  SetupValidationLayers();
  VK_CHECK_RESULT(volkInitialize());
  CreateInstance();
  volkLoadInstance(m_instance);

  CreateDevice(a_deviceId);
  volkLoadDevice(m_device);

//  vk_rt_utils::LoadRayTracingFunctions(m_device);

  GetRTFeatures();

  m_commandPool = vk_utils::createCommandPool(m_device, m_queueFamilyIDXs.graphics, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

  m_cmdBuffersDrawMain.reserve(m_framesInFlight);
  m_cmdBuffersDrawMain = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);

  m_frameFences.resize(m_framesInFlight);
  VkFenceCreateInfo fenceInfo = {};
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  for (size_t i = 0; i < m_framesInFlight; i++)
  {
    VK_CHECK_RESULT(vkCreateFence(m_device, &fenceInfo, nullptr, &m_frameFences[i]));
  }

  m_pCopyHelper = std::make_shared<vk_utils::PingPongCopyHelper>(m_physicalDevice, m_device, m_transferQueue,
    m_queueFamilyIDXs.transfer, scratchMemSize);

  LoaderConfig conf = {};
  conf.load_geometry = true;
  conf.load_materials = MATERIAL_LOAD_MODE::NONE;
  conf.build_acc_structs = true;
  conf.build_acc_structs_while_loading_scene = true;
  conf.builder_type = BVH_BUILDER_TYPE::RTX;

  m_pScnMgr = std::make_shared<SceneManager>(m_device, m_physicalDevice, m_queueFamilyIDXs.graphics, m_pCopyHelper, conf);
//  m_pScnMgr = std::make_shared<SceneManager>(m_device, m_physicalDevice, m_queueFamilyIDXs.transfer, m_queueFamilyIDXs.graphics, true, true);
}

void RenderEngineRayQuery::InitPresentation(VkSurfaceKHR& a_surface)
{
  m_surface = a_surface;

  // presentation resources
  //
  m_presentationResources.queue = m_swapchain.CreateSwapChain(m_physicalDevice, m_device, m_surface, m_width, m_height,
           m_framesInFlight,  m_vsync);
  m_presentationResources.currentFrame = 0;

  VkSemaphoreCreateInfo semaphoreInfo = {};
  semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
  VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_presentationResources.imageAvailable));
  VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_presentationResources.renderingFinished));

  vk_utils::getSupportedDepthFormat(m_physicalDevice, m_depthFormats, &m_depthBuffer.format);
  m_depthBuffer = vk_utils::createDepthTexture(m_device, m_physicalDevice, m_width, m_height, m_depthBuffer.format);
}

void RenderEngineRayQuery::CreateInstance()
{
  VkApplicationInfo appInfo = {};
  appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pNext              = nullptr;
  appInfo.pApplicationName   = "RT Render";
  appInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
  appInfo.pEngineName        = "ChimeraRT";
  appInfo.engineVersion      = VK_MAKE_VERSION(0, 1, 0);
  appInfo.apiVersion         = VK_MAKE_VERSION(1, 2, 0);

  m_instance = vk_utils::createInstance(m_enableValidation, m_validationLayers, m_instanceExtensions, &appInfo);

  if (m_enableValidation)
    vk_utils::initDebugReportCallback(m_instance, &debugReportCallbackFn, &m_debugReportCallback);
}

void RenderEngineRayQuery::CreateDevice(uint32_t a_deviceId)
{
  SetupDeviceExtensions();
  m_physicalDevice = vk_utils::findPhysicalDevice(m_instance, true, a_deviceId, m_deviceExtensions);

  SetupDeviceFeatures();
  m_device = vk_utils::createLogicalDevice(m_physicalDevice, m_validationLayers, m_deviceExtensions,
                             m_enabledDeviceFeatures, m_queueFamilyIDXs,
                          VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT | VK_QUEUE_COMPUTE_BIT,
                                           m_pDeviceFeatures);

  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.graphics, 0, &m_graphicsQueue);
  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.transfer, 0, &m_transferQueue);
  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.compute,  0, &m_computeQueue);
}

void RenderEngineRayQuery::SetupRTPipeline()
{
  std::vector<std::pair<VkDescriptorType, uint32_t> > dtypes = {
    {VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, 1},
    {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1},
    {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 2},
    {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1}
  };
  m_pBindings = std::make_shared<vk_utils::DescriptorMaker>(m_device, dtypes, 1);

  VkShaderStageFlags flags = VK_SHADER_STAGE_COMPUTE_BIT;
  m_pBindings->BindBegin(flags);
  m_pBindings->BindAccelStruct(0, m_pScnMgr->GetTLAS());
  m_pBindings->BindImage(1, m_storageImage.attachment.view, VK_NULL_HANDLE, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_IMAGE_LAYOUT_GENERAL);
  m_pBindings->BindBuffer(2, m_ubo, VK_NULL_HANDLE, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
  m_pBindings->BindBuffer(3, m_pScnMgr->GetVertexBuffer());
  m_pBindings->BindBuffer(4, m_pScnMgr->GetIndexBuffer());
  m_pBindings->BindBuffer(5, m_pScnMgr->GetMeshInfoBuffer());
  m_pBindings->BindEnd(&m_rtDS, &m_rtDSLayout);

  VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
  pipelineLayoutInfo.sType                  = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipelineLayoutInfo.pushConstantRangeCount = 0;
  pipelineLayoutInfo.pPushConstantRanges    = nullptr;
  pipelineLayoutInfo.pSetLayouts    = &m_rtDSLayout;
  pipelineLayoutInfo.setLayoutCount = 1;
  VK_CHECK_RESULT(vkCreatePipelineLayout(m_device, &pipelineLayoutInfo, nullptr, &m_rtPipeline.layout));

  std::vector<VkShaderModule> modules;
  VkPipelineShaderStageCreateInfo shaderStageCreateInfo = vk_utils::loadShader(m_device, "../resources/shaders/raytrace.comp.spv",
                                                                               VK_SHADER_STAGE_COMPUTE_BIT, modules);

  VkComputePipelineCreateInfo pipelineCreateInfo = { VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO };
  pipelineCreateInfo.stage                       = shaderStageCreateInfo;
  pipelineCreateInfo.layout                      = m_rtPipeline.layout;

  VK_CHECK_RESULT(vkCreateComputePipelines(m_device,
                                      VK_NULL_HANDLE,
                                      1, &pipelineCreateInfo,
                                      nullptr,
                                      &m_rtPipeline.pipeline));
}

void RenderEngineRayQuery::CreateUniformBuffer()
{
  VkMemoryRequirements memReq {};
  m_ubo = vk_utils::createBuffer(m_device, sizeof(UniformParams), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, &memReq);

  VkMemoryAllocateInfo allocateInfo = {};
  allocateInfo.sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  allocateInfo.pNext           = nullptr;
  allocateInfo.allocationSize  = memReq.size;
  allocateInfo.memoryTypeIndex = vk_utils::findMemoryType(memReq.memoryTypeBits,VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, m_physicalDevice);
  VK_CHECK_RESULT(vkAllocateMemory(m_device, &allocateInfo, nullptr, &m_uboAlloc));

  VK_CHECK_RESULT(vkBindBufferMemory(m_device, m_ubo, m_uboAlloc, 0));

  vkMapMemory(m_device, m_uboAlloc, 0, sizeof(m_uniforms), 0, &m_uboMappedMem);

  UpdateUniformBuffer();
}

void RenderEngineRayQuery::UpdateUniformBuffer()
{
  const float aspect = float(m_width) / float(m_height);
  auto mProjFix = OpenglToVulkanProjectionMatrixFix();
  auto mProj    = projectionMatrix(m_cam.fov, aspect, 0.1f, 1000.0f);
  auto mLookAt  = LiteMath::lookAt(m_cam.pos, m_cam.lookAt, m_cam.up);
//  auto mWorldViewProj = mul(mul(mLookAt, mProj), mProjFix);

  m_uniforms.viewInverse = LiteMath::inverse4x4(mLookAt);
  m_uniforms.projInverse = LiteMath::inverse4x4(mProjFix * mProj);
  m_uniforms.vertexSize  = sizeof(LiteMath::float4) * 2; //@TODO

  memcpy(m_uboMappedMem, &m_uniforms, sizeof(m_uniforms));
}

void RenderEngineRayQuery::CreateStorageImage()
{
  if (m_storageImage.attachment.image != VK_NULL_HANDLE)
  {
    vkDestroyImageView(m_device, m_storageImage.attachment.view, nullptr);
    vkDestroyImage(m_device, m_storageImage.attachment.image, nullptr);
    vkFreeMemory(m_device, m_storageImage.memory, nullptr);
    m_storageImage = {};
  }

  VkImageCreateInfo image  {VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
  image.imageType = VK_IMAGE_TYPE_2D;
  image.format = m_swapchain.GetFormat();
  image.extent = { m_width, m_height, 1 };
  image.mipLevels = 1;
  image.arrayLayers = 1;
  image.samples = VK_SAMPLE_COUNT_1_BIT;
  image.tiling = VK_IMAGE_TILING_OPTIMAL;
  image.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
  image.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  VK_CHECK_RESULT(vkCreateImage(m_device, &image, nullptr, &m_storageImage.attachment.image));

  VkMemoryRequirements memReqs;
  vkGetImageMemoryRequirements(m_device, m_storageImage.attachment.image, &memReqs);
  VkMemoryAllocateInfo memoryAllocateInfo {VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO};
  memoryAllocateInfo.allocationSize = memReqs.size;
  memoryAllocateInfo.memoryTypeIndex = vk_utils::findMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_physicalDevice);
  VK_CHECK_RESULT(vkAllocateMemory(m_device, &memoryAllocateInfo, nullptr, &m_storageImage.memory));
  VK_CHECK_RESULT(vkBindImageMemory(m_device, m_storageImage.attachment.image, m_storageImage.memory, 0));

  VkImageViewCreateInfo colorImageView { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
  colorImageView.viewType = VK_IMAGE_VIEW_TYPE_2D;
  colorImageView.format = m_swapchain.GetFormat();
  colorImageView.subresourceRange = {};
  colorImageView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  colorImageView.subresourceRange.baseMipLevel = 0;
  colorImageView.subresourceRange.levelCount = 1;
  colorImageView.subresourceRange.baseArrayLayer = 0;
  colorImageView.subresourceRange.layerCount = 1;
  colorImageView.image = m_storageImage.attachment.image;
  VK_CHECK_RESULT(vkCreateImageView(m_device, &colorImageView, nullptr, &m_storageImage.attachment.view));

  VkCommandBuffer cmdBuf = vk_utils::createCommandBuffers(m_device, m_commandPool, 1)[0];
  VkCommandBufferBeginInfo cmdBufInfo{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };

  VK_CHECK_RESULT(vkBeginCommandBuffer(cmdBuf, &cmdBufInfo));
  vk_utils::setImageLayout(cmdBuf, m_storageImage.attachment.image,
    VK_IMAGE_LAYOUT_UNDEFINED,
    VK_IMAGE_LAYOUT_GENERAL,
    { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 });
  vkEndCommandBuffer(cmdBuf);

  vk_utils::executeCommandBufferNow(cmdBuf, m_graphicsQueue, m_device);
}

void RenderEngineRayQuery::BuildCommandBufferRT(VkCommandBuffer cmdBuff, uint32_t idx)
{
  VkCommandBufferBeginInfo cmdBufInfo{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };

  VkImageSubresourceRange subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

  VK_CHECK_RESULT(vkBeginCommandBuffer(cmdBuff, &cmdBufInfo));

  vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_COMPUTE, m_rtPipeline.pipeline);
  vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_COMPUTE, m_rtPipeline.layout, 0, 1, &m_rtDS, 0, 0);

  vkCmdDispatch(cmdBuff, (uint32_t(m_width) + wgWidth - 1) / wgWidth,
                (uint32_t(m_height) + wgHeight - 1) / wgHeight, 1);

  // Prepare current swap chain image as transfer destination
  vk_utils::setImageLayout(
    cmdBuff,
    m_swapchain.GetAttachment(idx).image,
    VK_IMAGE_LAYOUT_UNDEFINED,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    subresourceRange);

  // Prepare ray tracing output image as transfer source
  vk_utils::setImageLayout(
    cmdBuff,
    m_storageImage.attachment.image,
    VK_IMAGE_LAYOUT_GENERAL,
    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
    subresourceRange);

  VkImageCopy copyRegion{};
  copyRegion.srcSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
  copyRegion.srcOffset = { 0, 0, 0 };
  copyRegion.dstSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
  copyRegion.dstOffset = { 0, 0, 0 };
  copyRegion.extent = { m_width, m_height, 1 };
  vkCmdCopyImage(cmdBuff, m_storageImage.attachment.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
    m_swapchain.GetAttachment(idx).image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copyRegion);

  // Transition swap chain image back for presentation
  vk_utils::setImageLayout(
    cmdBuff,
    m_swapchain.GetAttachment(idx).image,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    subresourceRange);

  // Transition ray tracing output image back to general layout
  vk_utils::setImageLayout(
    cmdBuff,
    m_storageImage.attachment.image,
    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
    VK_IMAGE_LAYOUT_GENERAL,
    subresourceRange);

  VK_CHECK_RESULT(vkEndCommandBuffer(cmdBuff));

}

void RenderEngineRayQuery::CleanupPipelineAndSwapchain()
{
  if(!m_cmdBuffersDrawMain.empty())
  {
    vkFreeCommandBuffers(m_device, m_commandPool, static_cast<uint32_t>(m_cmdBuffersDrawMain.size()), m_cmdBuffersDrawMain.data());
    m_cmdBuffersDrawMain.clear();
  }

  for (size_t i = 0; i < m_frameFences.size(); i++)
  {
    vkDestroyFence(m_device, m_frameFences[i], nullptr);
  }

  vkDestroyImageView(m_device, m_depthBuffer.view, nullptr);
  vkDestroyImage(m_device, m_depthBuffer.image, nullptr);

  m_swapchain.Cleanup();
}

void RenderEngineRayQuery::RecreateSwapChain()
{
  vkDeviceWaitIdle(m_device);

  CleanupPipelineAndSwapchain();
  m_presentationResources.queue = m_swapchain.CreateSwapChain(m_physicalDevice, m_device, m_surface, m_width, m_height,
    m_framesInFlight, m_vsync);

  vk_utils::getSupportedDepthFormat(m_physicalDevice, m_depthFormats, &m_depthBuffer.format);
  m_depthBuffer = vk_utils::createDepthTexture(m_device, m_physicalDevice, m_width, m_height, m_depthBuffer.format);

  m_frameFences.resize(m_framesInFlight);
  VkFenceCreateInfo fenceInfo = {};
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  for (size_t i = 0; i < m_framesInFlight; i++)
  {
    VK_CHECK_RESULT(vkCreateFence(m_device, &fenceInfo, nullptr, &m_frameFences[i]));
  }

  m_cmdBuffersDrawMain = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);
  for(size_t i = 0; i < m_swapchain.GetImageCount(); ++i)
  {
    BuildCommandBufferRT(m_cmdBuffersDrawMain[i], i);
  }
}

void RenderEngineRayQuery::UpdateCamera(const Camera &cam)
{
  m_cam = cam;
  UpdateUniformBuffer();
}

void RenderEngineRayQuery::LoadScene(const std::string &path, bool transpose_inst_matrices)
{
//  m_pScnMgr->LoadSceneXML(path, transpose_inst_matrices);
////  m_pScnMgr->LoadSingleTriangle();
//  m_pScnMgr->BuildAllBLAS();
//

  auto loaded = m_pScnMgr->LoadScene(path);

  if(!loaded)
  {
    RUN_TIME_ERROR("SCENE NOT LOADED");
  }
  m_pScnMgr->BuildTLAS();

  CreateUniformBuffer();
  CreateStorageImage();
  SetupRTPipeline();

  auto loadedCam = m_pScnMgr->GetCamera(0);
  m_cam.fov = loadedCam.fov;
  m_cam.pos = float3(loadedCam.pos);
  m_cam.up  = float3(loadedCam.up);
  m_cam.lookAt = float3(loadedCam.lookAt);
  m_cam.tdist  = loadedCam.farPlane;


  for(size_t i = 0; i < m_swapchain.GetImageCount(); ++i)
  {
    BuildCommandBufferRT(m_cmdBuffersDrawMain[i], i);
  }
}

void RenderEngineRayQuery::DrawFrame()
{
  vkWaitForFences(m_device, 1, &m_frameFences[m_presentationResources.currentFrame], VK_TRUE, UINT64_MAX);
  vkResetFences(m_device, 1, &m_frameFences[m_presentationResources.currentFrame]);

  uint32_t imageIdx;
  m_swapchain.AcquireNextImage(m_presentationResources.imageAvailable, &imageIdx);

  auto currentCmdBuf = m_cmdBuffersDrawMain[imageIdx];

  VkSemaphore      waitSemaphores[] = { m_presentationResources.imageAvailable };
  VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };

  BuildCommandBufferRT(currentCmdBuf, imageIdx);

  VkSubmitInfo submitInfo = {};
  submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores    = waitSemaphores;
  submitInfo.pWaitDstStageMask  = waitStages;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers    = &currentCmdBuf;

  VkSemaphore signalSemaphores[]  = { m_presentationResources.renderingFinished };
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores    = signalSemaphores;

  VK_CHECK_RESULT(vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, m_frameFences[m_presentationResources.currentFrame]));

  VkResult presentRes = m_swapchain.QueuePresent(m_presentationResources.queue, imageIdx, m_presentationResources.renderingFinished);

  if (presentRes == VK_ERROR_OUT_OF_DATE_KHR || presentRes == VK_SUBOPTIMAL_KHR)
  {
    RecreateSwapChain();
  }
  else if (presentRes != VK_SUCCESS)
  {
    RUN_TIME_ERROR("Failed to present swapchain image");
  }

  m_presentationResources.currentFrame = (m_presentationResources.currentFrame + 1) % m_framesInFlight;

  vkQueueWaitIdle(m_presentationResources.queue);
}