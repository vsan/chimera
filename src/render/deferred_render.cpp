#include "deferred_render.h"
#include "../utils/input_definitions.h"
#include <vk_pipeline.h>

RenderEngine::RenderEngine(uint32_t a_width, uint32_t a_height) :  m_width(a_width), m_height(a_height)
{
#ifdef NDEBUG
  m_enableValidation = false;
#else
  m_enableValidation = true;
#endif

}

void RenderEngine::SetupDeviceFeatures()
{
  m_enabledDeviceFeatures.fillModeNonSolid = VK_TRUE;
  m_enabledDeviceFeatures.geometryShader   = VK_TRUE;
}

void RenderEngine::SetupDeviceExtensions()
{
  m_deviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
}

void RenderEngine::SetupValidationLayers()
{
  m_validationLayers.push_back("VK_LAYER_KHRONOS_validation");
  m_validationLayers.push_back("VK_LAYER_LUNARG_monitor");

  //useful layers: VK_LAYER_LUNARG_standard_validation, VK_LAYER_LUNARG_monitor, VK_LAYER_LUNARG_api_dump, VK_LAYER_KHRONOS_validation
}

void RenderEngine::InitVulkan(std::vector<const char*> a_instanceExtensions, uint32_t a_deviceId)
{
  m_instanceExtensions = std::move(a_instanceExtensions);
  SetupValidationLayers();
  VK_CHECK_RESULT(volkInitialize());
  CreateInstance();
  volkLoadInstance(m_instance);

  CreateDevice(a_deviceId);
  volkLoadDevice(m_device);

  m_commandPool = vk_utils::createCommandPool(m_device, m_queueFamilyIDXs.graphics, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

  m_cmdBuffersDrawMain.reserve(m_framesInFlight);
  m_cmdBuffersDrawMain = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);

  m_cmdBuffersDrawWire.reserve(m_framesInFlight);
  m_cmdBuffersDrawWire = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);

  m_cmdBuffersDrawOffscreen.reserve(m_framesInFlight);
  m_cmdBuffersDrawOffscreen = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);

  m_frameFences.resize(m_framesInFlight);
  VkFenceCreateInfo fenceInfo = {};
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  for (size_t i = 0; i < m_framesInFlight; i++)
  {
    VK_CHECK_RESULT(vkCreateFence(m_device, &fenceInfo, nullptr, &m_frameFences[i]));
  }

  m_pCopyHelper = std::make_shared<vk_utils::PingPongCopyHelper>(m_physicalDevice, m_device, m_transferQueue,
    m_queueFamilyIDXs.transfer, scratchMemSize);

  LoaderConfig conf = {};
  conf.load_geometry = true;
  conf.load_materials = MATERIAL_LOAD_MODE::MATERIALS_AND_TEXTURES;
  conf.instance_matrix_as_vertex_attribute = true;
  m_pScnMgr = std::make_shared<SceneManager>(m_device, m_physicalDevice, m_queueFamilyIDXs.graphics, m_pCopyHelper, conf);
}

void RenderEngine::InitPresentation(VkSurfaceKHR& a_surface)
{
  m_surface = a_surface;

  // presentation resources
  //
  m_presentationResources.queue = m_swapchain.CreateSwapChain(m_physicalDevice, m_device, m_surface, m_width, m_height,
           m_framesInFlight, m_vsync);
  m_presentationResources.currentFrame = 0;

  VkSemaphoreCreateInfo semaphoreInfo = {};
  semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
  VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_presentationResources.imageAvailable));
  VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_presentationResources.renderingFinished));
  VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_offscreenSemaphore));
  m_screenRenderPass = vk_utils::createDefaultRenderPass(m_device, m_swapchain.GetFormat());

  vk_utils::getSupportedDepthFormat(m_physicalDevice, m_depthFormats, &m_depthBuffer.format);
  m_depthBuffer = vk_utils::createDepthTexture(m_device, m_physicalDevice, m_width, m_height, m_depthBuffer.format);

  m_frameBuffers = vk_utils::createFrameBuffers(m_device, m_swapchain, m_screenRenderPass, m_depthBuffer.view);

}

void RenderEngine::CreateInstance()
{
  VkApplicationInfo appInfo  = {};
  appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pNext              = nullptr;
  appInfo.pApplicationName   = "Render";
  appInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
  appInfo.pEngineName        = "Chimera";
  appInfo.engineVersion      = VK_MAKE_VERSION(0, 1, 0);
  appInfo.apiVersion         = VK_MAKE_VERSION(1, 2, 0);

  m_instance = vk_utils::createInstance(m_enableValidation, m_validationLayers, m_instanceExtensions, &appInfo);

  if (m_enableValidation)
    vk_utils::initDebugReportCallback(m_instance, &debugReportCallbackFn, &m_debugReportCallback);
}

void RenderEngine::CreateDevice(uint32_t a_deviceId)
{
  SetupDeviceExtensions();
  m_physicalDevice = vk_utils::findPhysicalDevice(m_instance, true, a_deviceId, m_deviceExtensions);

  SetupDeviceFeatures();
  m_device = vk_utils::createLogicalDevice(m_physicalDevice, m_validationLayers, m_deviceExtensions,
    m_enabledDeviceFeatures, m_queueFamilyIDXs, VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT | VK_QUEUE_COMPUTE_BIT);

  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.graphics, 0, &m_graphicsQueue);
  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.transfer, 0, &m_transferQueue);
  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.compute,  0, &m_computeQueue);
}

void RenderEngine::SetupQuadPipeline()
{
  std::vector<std::pair<VkDescriptorType, uint32_t> > dtypes = {
    {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1},
    {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 4}
  };

  vk_utils::destroyPipelineIfExists(m_device, m_gbufPipeline.pipeline, m_gbufPipeline.layout);

  m_pBindings = std::make_shared<vk_utils::DescriptorMaker>(m_device, dtypes, 2);

  m_pBindings->BindBegin(VK_SHADER_STAGE_FRAGMENT_BIT);
  m_pBindings->BindEnd(&m_GBufDS, &m_GBufDSLayout);

  vk_utils::GraphicsPipelineMaker maker;

  std::unordered_map<VkShaderStageFlagBits, std::string> shader_paths;
  shader_paths[VK_SHADER_STAGE_FRAGMENT_BIT] = "../resources/shaders/gbuf.frag.spv";
  shader_paths[VK_SHADER_STAGE_VERTEX_BIT] = "../resources/shaders/gbuf.vert.spv";

  maker.LoadShaders(m_device, shader_paths);

  m_gbufPipeline.layout = maker.MakeLayout(m_device, {m_GBufDSLayout}, sizeof(pushConsts));
  maker.SetDefaultState(m_width, m_height);
  VkPipelineColorBlendAttachmentState colorBlendAttachmentState {};
  colorBlendAttachmentState.colorWriteMask = vk_utils::ALL_COLOR_COMPONENTS;
  colorBlendAttachmentState.blendEnable = VK_FALSE;

  std::vector<VkPipelineColorBlendAttachmentState> blendAttachmentStates(m_GBufTarget->GetNumColorAttachments());
  for(auto& state : blendAttachmentStates)
    state = colorBlendAttachmentState;

  maker.colorBlending.attachmentCount = static_cast<uint32_t>(blendAttachmentStates.size());
  maker.colorBlending.pAttachments = blendAttachmentStates.data();
  maker.rasterizer.cullMode = VK_CULL_MODE_NONE;
  m_gbufPipeline.pipeline = maker.MakePipeline(m_device, m_pScnMgr->GetPipelineVertexInputStateCreateInfo(), m_GBufTarget->m_renderPass);

  //****************
  vk_utils::destroyPipelineIfExists(m_device, m_quadPipeline.pipeline, m_quadPipeline.layout);

  m_pBindings->BindBegin(VK_SHADER_STAGE_FRAGMENT_BIT);
  m_pBindings->BindImage(0, m_GBufTarget->m_attachments[m_GBuf_idx[GBUF_ATTACHMENT::POS_Z]].view, m_GBufTarget->m_sampler,
                         VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER);
  m_pBindings->BindImage(1, m_GBufTarget->m_attachments[m_GBuf_idx[GBUF_ATTACHMENT::NORMAL]].view, m_GBufTarget->m_sampler,
                         VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER);
  m_pBindings->BindImage(2, m_GBufTarget->m_attachments[m_GBuf_idx[GBUF_ATTACHMENT::ALBEDO]].view, m_GBufTarget->m_sampler,
                         VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER);
  m_pBindings->BindEnd(&m_quadDS, &m_quadDSLayout);

  shader_paths[VK_SHADER_STAGE_FRAGMENT_BIT] = "../resources/shaders/quad.frag.spv";
  shader_paths[VK_SHADER_STAGE_VERTEX_BIT] = "../resources/shaders/quad.vert.spv";

  maker.LoadShaders(m_device, shader_paths);
  m_quadPipeline.layout = maker.MakeLayout(m_device, {m_quadDSLayout}, sizeof(pushConsts));
  maker.SetDefaultState(m_width, m_height);

  maker.rasterizer.cullMode = VK_CULL_MODE_NONE;

  VkPipelineVertexInputStateCreateInfo emptyVertexInputState {};
  emptyVertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

  m_quadPipeline.pipeline = maker.MakePipeline(m_device, emptyVertexInputState, m_screenRenderPass);
}

void RenderEngine::SetupSimplePipeline()
{
  std::vector<std::pair<VkDescriptorType, uint32_t> > dtypes = {
    {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1}
  };
  vk_utils::destroyPipelineIfExists(m_device, m_basicForwardPipeline.pipeline, m_basicForwardPipeline.layout);

  m_pBindings = std::make_shared<vk_utils::DescriptorMaker>(m_device, dtypes, 1);

  m_pBindings->BindBegin(VK_SHADER_STAGE_FRAGMENT_BIT);
  m_pBindings->BindEnd(&m_GBufDS, &m_GBufDSLayout);

  vk_utils::GraphicsPipelineMaker maker;

  std::unordered_map<VkShaderStageFlagBits, std::string> shader_paths;
  shader_paths[VK_SHADER_STAGE_FRAGMENT_BIT] = "../resources/shaders/simple.frag.spv";
  shader_paths[VK_SHADER_STAGE_VERTEX_BIT] = "../resources/shaders/simple.vert.spv";

  maker.LoadShaders(m_device, shader_paths);

  m_basicForwardPipeline.layout = maker.MakeLayout(m_device, {m_GBufDSLayout}, sizeof(pushConsts));
  maker.SetDefaultState(m_width, m_height);

  maker.pipelineInfo.flags              = VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;
  maker.pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
  maker.pipelineInfo.basePipelineIndex  = -1;
  m_basicForwardPipeline.pipeline = maker.MakePipeline(m_device, m_pScnMgr->GetPipelineVertexInputStateCreateInfo(), m_screenRenderPass);

  // wireframe ***********************
  shader_paths[VK_SHADER_STAGE_FRAGMENT_BIT] = "../resources/shaders/wireframe.frag.spv";
  shader_paths[VK_SHADER_STAGE_VERTEX_BIT] = "../resources/shaders/wireframe.vert.spv";

  maker.LoadShaders(m_device, shader_paths);
  m_wireframePipeline.layout = maker.MakeLayout(m_device, {m_GBufDSLayout}, sizeof(pushConsts));
  maker.SetDefaultState(m_width, m_height);
  // setup derivative pipeline differences
  {
    maker.pipelineInfo.flags              = VK_PIPELINE_CREATE_DERIVATIVE_BIT;
    maker.pipelineInfo.basePipelineHandle = m_basicForwardPipeline.pipeline;
    maker.pipelineInfo.basePipelineIndex  = -1;
    maker.rasterizer.polygonMode = VK_POLYGON_MODE_LINE;
    maker.rasterizer.cullMode = VK_CULL_MODE_NONE;
    maker.rasterizer.lineWidth = 1.0f;
    maker.colorBlendAttachments[0].blendEnable = VK_FALSE;
  }

  m_wireframePipeline.pipeline = maker.MakePipeline(m_device, m_pScnMgr->GetPipelineVertexInputStateCreateInfo(), m_screenRenderPass);
}

void RenderEngine::SetupPbrForwardPipeline()
{
  std::vector<std::pair<VkDescriptorType, uint32_t> > dtypes = {
    {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 3},
    {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, MAX_TEXTURES}
  };
  vk_utils::destroyPipelineIfExists(m_device, m_pbrForwardPipeline.pipeline, m_pbrForwardPipeline.layout);

  m_pBindings = std::make_shared<vk_utils::DescriptorMaker>(m_device, dtypes, 1);

  m_pBindings->BindBegin(VK_SHADER_STAGE_FRAGMENT_BIT);
  m_pBindings->BindBuffer(0, m_pScnMgr->GetMaterialsBuffer());
  m_pBindings->BindBuffer(1, m_pScnMgr->GetMaterialIDsBuffer());
  m_pBindings->BindImageArray(2, m_pScnMgr->GetTextureViews(), m_pScnMgr->GetTextureSamplers());
  m_pBindings->BindBuffer(3, m_pScnMgr->GetMeshInfoBuffer());
  m_pBindings->BindEnd(&m_pbrForwardDS, &m_pbrForwardDSLayout);

  vk_utils::GraphicsPipelineMaker maker;

  std::unordered_map<VkShaderStageFlagBits, std::string> shader_paths;
  shader_paths[VK_SHADER_STAGE_FRAGMENT_BIT] = "../resources/shaders/pbrMetallicRoughnessForwardSpec.frag.spv";
  shader_paths[VK_SHADER_STAGE_VERTEX_BIT] = "../resources/shaders/simple.vert.spv";

  maker.LoadShaders(m_device, shader_paths);

  m_pbrForwardPipeline.layout = maker.MakeLayout(m_device, {m_pbrForwardDSLayout}, sizeof(pushConsts));
  maker.SetDefaultState(m_width, m_height);

  m_pbrForwardPipeline.pipeline = maker.MakePipeline(m_device, m_pScnMgr->GetPipelineVertexInputStateCreateInfo(), m_screenRenderPass);

}


void RenderEngine::BuildCommandBufferForward(VkCommandBuffer a_cmdBuff, VkFramebuffer a_frameBuff, pipeline_data_t a_pipeline, VkDescriptorSet a_dSet)
{
  vkResetCommandBuffer(a_cmdBuff, 0);

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

  VK_CHECK_RESULT(vkBeginCommandBuffer(a_cmdBuff, &beginInfo));

  vk_utils::setDefaultViewport(a_cmdBuff, static_cast<float>(m_width), static_cast<float>(m_height));
  vk_utils::setDefaultScissor(a_cmdBuff, m_width, m_height);

  ///// draw final scene to screen
  {
    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType             = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass        = m_screenRenderPass;
    renderPassInfo.framebuffer       = a_frameBuff;
    renderPassInfo.renderArea.offset = { 0, 0 };
    renderPassInfo.renderArea.extent = m_swapchain.GetExtent();

    VkClearValue clearValues[2] = {};
    clearValues[0].color           = {0.0f, 0.0f, 0.0f, 1.0f};
    clearValues[1].depthStencil    = {1.0f, 0};
    renderPassInfo.clearValueCount = 2;
    renderPassInfo.pClearValues    = &clearValues[0];

    vkCmdBeginRenderPass(a_cmdBuff, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline   (a_cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, a_pipeline.pipeline);

    if(a_dSet != VK_NULL_HANDLE)
      vkCmdBindDescriptorSets(a_cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, a_pipeline.layout, 0, 1,
                              &a_dSet, 0, NULL);

    VkShaderStageFlags stageFlags = (VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);

    VkDeviceSize zero_offset = 0u;
    VkBuffer vertexBuf = m_pScnMgr->GetVertexBuffer();
    VkBuffer indexBuf  = m_pScnMgr->GetIndexBuffer();
    VkBuffer instBuf   = m_pScnMgr->GetInstanceMatBuffer();

    vkCmdBindVertexBuffers(a_cmdBuff, 0, 1, &vertexBuf, &zero_offset);
    vkCmdBindIndexBuffer  (a_cmdBuff, indexBuf, 0, VK_INDEX_TYPE_UINT32);
    vkCmdBindVertexBuffers(a_cmdBuff, 1, 1, &instBuf, &zero_offset);

    for(size_t i = 0; i < m_pScnMgr->InstancesNum(); ++i)
    {
      auto inst = m_pScnMgr->GetInstanceInfo(i);

//      pushConsts.mModel = m_pScnMgr->GetInstanceMatrix(i);
      pushConsts.instanceID = inst.mesh_id;
      vkCmdPushConstants(a_cmdBuff, a_pipeline.layout, stageFlags, 0, sizeof(pushConsts), &pushConsts);

      auto mesh_info = m_pScnMgr->GetMeshInfo(inst.mesh_id);
      vkCmdDrawIndexed(a_cmdBuff, mesh_info.m_indNum, 1, mesh_info.m_indexOffset, mesh_info.m_vertexOffset, inst.inst_id);
    }

    vkCmdEndRenderPass(a_cmdBuff);
  }

  VK_CHECK_RESULT(vkEndCommandBuffer(a_cmdBuff));
}

void RenderEngine::BuildCommandBufferOffscreen(VkCommandBuffer a_cmdBuff, uint32_t a_fbufIdx)
{
  vkResetCommandBuffer(a_cmdBuff, 0);

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  VK_CHECK_RESULT(vkBeginCommandBuffer(a_cmdBuff, &beginInfo));

  vk_utils::setDefaultViewport(a_cmdBuff, static_cast<float>(m_width), static_cast<float>(m_height));
  vk_utils::setDefaultScissor(a_cmdBuff, m_width, m_height);

  // offscreen rendering - fill G-Buffer
  {
    std::vector<VkClearValue> clearValues(m_GBufTarget->m_attachments.size());
    for(size_t i = 0; i < m_GBufTarget->GetNumColorAttachments(); ++i)
    {
      clearValues[i].color = {0.0f, 0.0f, 0.0f, 1.0f};
    }
    if(m_GBufTarget->m_attachments.size() > m_GBufTarget->GetNumColorAttachments())
      clearValues.back().depthStencil = {1.0f, 0};

    VkRenderPassBeginInfo renderPassInfo = m_GBufTarget->GetRenderPassBeginInfo(a_fbufIdx, clearValues);

    vkCmdBeginRenderPass(a_cmdBuff, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline   (a_cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, m_gbufPipeline.pipeline);

    vkCmdBindDescriptorSets(a_cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, m_gbufPipeline.layout, 0, 1,
                            &m_GBufDS, 0, nullptr);

    VkShaderStageFlags stageFlags = (VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);

    VkDeviceSize zero_offset = 0u;
    VkBuffer vertexBuf = m_pScnMgr->GetVertexBuffer();
    VkBuffer indexBuf  = m_pScnMgr->GetIndexBuffer();
    VkBuffer instBuf   = m_pScnMgr->GetInstanceMatBuffer();

    vkCmdBindVertexBuffers(a_cmdBuff, 0, 1, &vertexBuf, &zero_offset);
    vkCmdBindIndexBuffer  (a_cmdBuff, indexBuf, 0, VK_INDEX_TYPE_UINT32);
    vkCmdBindVertexBuffers(a_cmdBuff, 1, 1, &instBuf, &zero_offset);

    for(size_t i = 0; i < m_pScnMgr->InstancesNum(); ++i)
    {
      auto inst = m_pScnMgr->GetInstanceInfo(i);

//      pushConsts.mModel = m_pScnMgr->GetInstanceMatrix(i);
      vkCmdPushConstants(a_cmdBuff, m_gbufPipeline.layout, stageFlags, 0, sizeof(pushConsts), &pushConsts);

      auto mesh_info = m_pScnMgr->GetMeshInfo(inst.mesh_id);
      vkCmdDrawIndexed(a_cmdBuff, mesh_info.m_indNum, 1, mesh_info.m_indexOffset, mesh_info.m_vertexOffset, inst.inst_id);
    }

    vkCmdEndRenderPass(a_cmdBuff);
  }

  VK_CHECK_RESULT(vkEndCommandBuffer(a_cmdBuff));
}

void RenderEngine::BuildCommandBufferQuad(VkCommandBuffer a_cmdBuff, VkFramebuffer a_frameBuff)
{
  vkResetCommandBuffer(a_cmdBuff, 0);

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  VK_CHECK_RESULT(vkBeginCommandBuffer(a_cmdBuff, &beginInfo));

  vk_utils::setDefaultViewport(a_cmdBuff, static_cast<float>(m_width), static_cast<float>(m_height));
  vk_utils::setDefaultScissor(a_cmdBuff, m_width, m_height);

 // render fullscreen quad
  {
    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType             = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass        = m_screenRenderPass;
    renderPassInfo.framebuffer       = a_frameBuff;
    renderPassInfo.renderArea.offset = { 0, 0 };
    renderPassInfo.renderArea.extent = m_swapchain.GetExtent();

    VkClearValue clearValues[2] = {};
    clearValues[0].color           = {0.0f, 0.0f, 0.0f, 1.0f};
    clearValues[1].depthStencil    = {1.0f, 0};
    renderPassInfo.clearValueCount = 2;
    renderPassInfo.pClearValues    = &clearValues[0];

    vkCmdBeginRenderPass(a_cmdBuff, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline   (a_cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, m_quadPipeline.pipeline);

    vkCmdBindDescriptorSets(a_cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, m_quadPipeline.layout, 0, 1,
                            &m_quadDS, 0, nullptr);

    vkCmdDraw(a_cmdBuff, 3, 1, 0, 0);

    vkCmdEndRenderPass(a_cmdBuff);
  }

  VK_CHECK_RESULT(vkEndCommandBuffer(a_cmdBuff));
}

void RenderEngine::CleanupPipelineAndSwapchain()
{
  if(!m_cmdBuffersDrawMain.empty())
  {
    vkFreeCommandBuffers(m_device, m_commandPool, static_cast<uint32_t>(m_cmdBuffersDrawMain.size()), m_cmdBuffersDrawMain.data());
    m_cmdBuffersDrawMain.clear();
  }
  if(!m_cmdBuffersDrawWire.empty())
  {
    vkFreeCommandBuffers(m_device, m_commandPool, static_cast<uint32_t>(m_cmdBuffersDrawWire.size()), m_cmdBuffersDrawWire.data());
    m_cmdBuffersDrawWire.clear();
  }

  for (size_t i = 0; i < m_frameFences.size(); i++)
  {
    vkDestroyFence(m_device, m_frameFences[i], nullptr);
  }

  vkDestroyImageView(m_device, m_depthBuffer.view, nullptr);
  vkDestroyImage(m_device, m_depthBuffer.image, nullptr);

  for (size_t i = 0; i < m_frameBuffers.size(); i++)
  {
    vkDestroyFramebuffer(m_device, m_frameBuffers[i], nullptr);
  }

  vkDestroyRenderPass(m_device, m_screenRenderPass, nullptr);

  m_swapchain.Cleanup();
}

void RenderEngine::RecreateSwapChain()
{
  vkDeviceWaitIdle(m_device);

  CleanupPipelineAndSwapchain();
  m_presentationResources.queue = m_swapchain.CreateSwapChain(m_physicalDevice, m_device, m_surface, m_width, m_height,
    m_framesInFlight, m_vsync);

  m_screenRenderPass = vk_utils::createDefaultRenderPass(m_device, m_swapchain.GetFormat());

  vk_utils::getSupportedDepthFormat(m_physicalDevice, m_depthFormats, &m_depthBuffer.format);
  m_depthBuffer = vk_utils::createDepthTexture(m_device, m_physicalDevice, m_width, m_height, m_depthBuffer.format);

  m_frameBuffers = vk_utils::createFrameBuffers(m_device, m_swapchain, m_screenRenderPass, m_depthBuffer.view);

  m_frameFences.resize(m_framesInFlight);
  VkFenceCreateInfo fenceInfo = {};
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  for (size_t i = 0; i < m_framesInFlight; i++)
  {
    VK_CHECK_RESULT(vkCreateFence(m_device, &fenceInfo, nullptr, &m_frameFences[i]));
  }

  m_cmdBuffersDrawMain = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);
  m_cmdBuffersDrawWire = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);
  for(size_t i = 0; i < m_swapchain.GetImageCount(); ++i)
  {
    if(m_renderSettings.gbuf_mode)
    {
      BuildCommandBufferQuad(m_cmdBuffersDrawMain[i], m_frameBuffers[i]);
    }
    else
    {
      BuildCommandBufferForward(m_cmdBuffersDrawMain[i], m_frameBuffers[i], m_pbrForwardPipeline, m_pbrForwardDS);
      BuildCommandBufferForward(m_cmdBuffersDrawWire[i], m_frameBuffers[i], m_wireframePipeline, VK_NULL_HANDLE);
    }
  }

}

void RenderEngine::Cleanup()
{
  CleanupPipelineAndSwapchain();

  if(m_attachmentsMem != VK_NULL_HANDLE)
    vkFreeMemory(m_device, m_attachmentsMem, nullptr);

  if(!m_cmdBuffersDrawOffscreen.empty())
  {
    vkFreeCommandBuffers(m_device, m_commandPool, static_cast<uint32_t>(m_cmdBuffersDrawOffscreen.size()), m_cmdBuffersDrawOffscreen.data());
    m_cmdBuffersDrawOffscreen.clear();
  }

  if(m_offscreenSemaphore != VK_NULL_HANDLE)
  {
    vkDestroySemaphore(m_device, m_offscreenSemaphore, nullptr);
  }

  if(m_gbufPipeline.pipeline != VK_NULL_HANDLE)
  {
    vkDestroyPipeline(m_device, m_gbufPipeline.pipeline, nullptr);
  }
  if(m_gbufPipeline.layout != VK_NULL_HANDLE)
  {
    vkDestroyPipelineLayout(m_device, m_gbufPipeline.layout, nullptr);
  }

  if(m_quadPipeline.pipeline != VK_NULL_HANDLE)
  {
    vkDestroyPipeline(m_device, m_quadPipeline.pipeline, nullptr);
  }
  if(m_quadPipeline.layout != VK_NULL_HANDLE)
  {
    vkDestroyPipelineLayout(m_device, m_quadPipeline.layout, nullptr);
  }

  if(m_wireframePipeline.pipeline != VK_NULL_HANDLE)
  {
    vkDestroyPipeline(m_device, m_wireframePipeline.pipeline, nullptr);
  }
  if(m_wireframePipeline.layout != VK_NULL_HANDLE)
  {
    vkDestroyPipelineLayout(m_device, m_wireframePipeline.layout, nullptr);
  }

  if(m_basicForwardPipeline.pipeline != VK_NULL_HANDLE)
  {
    vkDestroyPipeline(m_device, m_basicForwardPipeline.pipeline, nullptr);
  }
  if(m_basicForwardPipeline.layout != VK_NULL_HANDLE)
  {
    vkDestroyPipelineLayout(m_device, m_basicForwardPipeline.layout, nullptr);
  }

  if(m_presentationResources.imageAvailable != VK_NULL_HANDLE)
  {
    vkDestroySemaphore(m_device, m_presentationResources.imageAvailable, nullptr);
  }
  if(m_presentationResources.renderingFinished != VK_NULL_HANDLE)
  {
    vkDestroySemaphore(m_device, m_presentationResources.renderingFinished, nullptr);
  }

  if(m_commandPool != VK_NULL_HANDLE)
  {
    vkDestroyCommandPool(m_device, m_commandPool, nullptr);
  }
}

void RenderEngine::ProcessInput(AppInput& input)
{
  if(input.keyPressed[GLFW_KEY_SPACE])
    m_renderSettings.wireframe_mode = !m_renderSettings.wireframe_mode;

  if(input.keyPressed[GLFW_KEY_1])
  {
    LoadScene(m_initialScenePath, false);
    input.cameraInternalUpdate = true;
  }
  if(input.keyPressed[GLFW_KEY_2])
  {
    LoadScene(DEFAULT_SCENE_PATH, false);
    input.cameraInternalUpdate = true;
  }
  if(input.keyPressed[GLFW_KEY_F12])
  {
    if(m_renderSettings.gbuf_mode)
    {
      {
        auto normal_img = m_GBufTarget->m_attachments[m_GBuf_idx[GBUF_ATTACHMENT::NORMAL]].image;
        std::vector<unsigned char> tmp(m_width * m_height * 4, 0);
        m_pCopyHelper->ReadImage(normal_img, tmp.data(), m_width, m_height, 4, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
        saveImageLDR("gbuffer_normals.png", tmp, m_width, m_height, 4);
      }

      {
        auto pos_img = m_GBufTarget->m_attachments[m_GBuf_idx[GBUF_ATTACHMENT::POS_Z]].image;
        std::vector<float> tmp(m_width * m_height * 4, 0);
        m_pCopyHelper->ReadImage(pos_img, tmp.data(), m_width, m_height, sizeof(float) * 4, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
        saveImageHDR("gbuffer_pos.hdr", tmp, m_width, m_height, 4);
      }
    }
    else
    {
      auto swapchain_img = m_swapchain.GetAttachment(0).image;
      std::vector<unsigned char> tmp(m_width * m_height * 4, 0);
      m_pCopyHelper->ReadImage(swapchain_img, tmp.data(), m_width, m_height, 4, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
      saveImageLDR("screenshot.png", tmp, m_width, m_height, 4);
    }
  }

}

void RenderEngine::UpdateCamera(const Camera &cam)
{
  m_cam = cam;
  UpdateView();
}

void RenderEngine::UpdateView()
{
  const float aspect = float(m_width) / float(m_height);
  auto mProjFix = OpenglToVulkanProjectionMatrixFix();
  auto mProj    = projectionMatrix(m_cam.fov, aspect, 0.1f, 1000.0f);
  auto mLookAt  = LiteMath::lookAt(m_cam.pos, m_cam.lookAt, m_cam.up);
  auto mWorldViewProj = mProjFix * mProj * mLookAt;

  pushConsts.viewPos  = m_cam.pos;
  pushConsts.mProjView = mWorldViewProj;
}

void RenderEngine::SetupOffscreenRendering()
{
  m_GBufTarget->m_device = m_device;

  vk_utils::AttachmentInfo info_posZ;
  info_posZ.format = VK_FORMAT_R32G32B32A32_SFLOAT;
  info_posZ.usage  = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
  info_posZ.imageSampleCount = VK_SAMPLE_COUNT_1_BIT;
  m_GBuf_idx[GBUF_ATTACHMENT::POS_Z] = m_GBufTarget->CreateAttachment(info_posZ);

  vk_utils::AttachmentInfo info_normal;
  info_normal.format = VK_FORMAT_R8G8B8A8_UNORM;
  info_normal.usage  = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
  info_normal.imageSampleCount = VK_SAMPLE_COUNT_1_BIT;
  m_GBuf_idx[GBUF_ATTACHMENT::NORMAL] = m_GBufTarget->CreateAttachment(info_normal);

  vk_utils::AttachmentInfo info_albedo = info_normal;
  m_GBuf_idx[GBUF_ATTACHMENT::ALBEDO] = m_GBufTarget->CreateAttachment(info_albedo);

  vk_utils::AttachmentInfo info_depth;
  info_depth.format = m_depthBuffer.format;
  info_depth.usage  = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
  info_depth.imageSampleCount = VK_SAMPLE_COUNT_1_BIT;
  m_GBufTarget->CreateAttachment(info_depth);

  std::vector<VkMemoryRequirements> memReqs = m_GBufTarget->GetMemoryRequirements();
  std::vector<VkDeviceSize> offsets = vk_utils::calculateMemOffsets(memReqs);

  VkMemoryAllocateInfo memAllocInfo {};
  memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  memAllocInfo.allocationSize = offsets.back();
  offsets.pop_back();
  memAllocInfo.memoryTypeIndex = vk_utils::findMemoryType(m_GBufTarget->m_attachments[0].mem_req.memoryTypeBits,
    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_physicalDevice);
  VK_CHECK_RESULT(vkAllocateMemory(m_device, &memAllocInfo, nullptr, &m_attachmentsMem));

  m_GBufTarget->CreateViewAndBindMemory(m_attachmentsMem, offsets);

  VK_CHECK_RESULT(m_GBufTarget->CreateDefaultSampler());
  VK_CHECK_RESULT(m_GBufTarget->CreateDefaultRenderPass());
}

void RenderEngine::LoadScene(const std::string &path, bool transpose_inst_matrices)
{
  if(m_sceneReload)
  {
    m_pScnMgr->DestroyScene();
  }
  else
  {
    m_initialScenePath = path;
  }

//  std::string ext = path.substr(path.find_last_of('.'), path.size());
//
//  if(ext == ".gltf")
//    m_pScnMgr->LoadSceneGLTF(path);
//  else if(ext == ".xml")
//    m_pScnMgr->LoadSceneXML(path, transpose_inst_matrices);

  m_pScnMgr->LoadScene(path);


  if(m_renderSettings.gbuf_mode)
  {
    if(m_GBufTarget)
      m_GBufTarget = nullptr;

    m_GBufTarget = std::make_unique<vk_utils::RenderTarget>(m_device, VkExtent2D{ m_width, m_height });
    SetupOffscreenRendering();
    SetupQuadPipeline();
  }
  else
  {
    SetupSimplePipeline();
    SetupPbrForwardPipeline();
  }


  auto loadedCam = m_pScnMgr->GetCamera(0);
  m_cam.fov = loadedCam.fov;
  m_cam.pos = float3(loadedCam.pos);
  m_cam.up  = float3(loadedCam.up);
  m_cam.lookAt = float3(loadedCam.lookAt);
  m_cam.tdist  = loadedCam.farPlane;
  UpdateView();

  for(size_t i = 0; i < m_framesInFlight; ++i)
  {
    if(m_renderSettings.gbuf_mode)
    {
      BuildCommandBufferOffscreen(m_cmdBuffersDrawOffscreen[i], 0);
      BuildCommandBufferQuad(m_cmdBuffersDrawMain[i], m_frameBuffers[i]);
    }
    else
    {
      BuildCommandBufferForward(m_cmdBuffersDrawMain[i], m_frameBuffers[i], m_pbrForwardPipeline, m_pbrForwardDS);
      BuildCommandBufferForward(m_cmdBuffersDrawWire[i], m_frameBuffers[i], m_wireframePipeline, VK_NULL_HANDLE);
    }
  }

  m_sceneReload = true;
}

//void RenderEngine::LoadTestTriangle()
//{
//  float normal[3]  = { 0.0f, 0.0f, 1.0f };
//  float tangent[3] = { 1.0f, 0.0f, 0.0f };
//  float triangleData [8 * 3] =
//    {
//      -1.0f, 0.0f, 0.0f, as_float(EncodeNormal(normal)),
//      0.0f, 0.0f, as_float(EncodeNormal(tangent)), 0.0f,
//
//      1.0f, 0.0f, 0.0f, as_float(EncodeNormal(normal)),
//      1.0f, 0.0f, as_float(EncodeNormal(tangent)), 0.0f,
//
//      0.0f, 1.0f, 0.0f, as_float(EncodeNormal(normal)),
//      0.5f, 1.0f, as_float(EncodeNormal(tangent)), 0.0f,
//    };
//  uint32_t indices[3] = {0, 1, 2};
//
//  MeshInfo triInfo;
//  triInfo.m_indNum = 3;
//  triInfo.m_vertNum = 3;
//
//  auto idxMemOffset = SetupGeoBuffers(sizeof(float) * 24, sizeof(uint32_t) * 3);
//
//  VK_CHECK_RESULT(vkBindBufferMemory(m_device, m_geoVertBuf, m_geoMemAlloc, 0));
//  VK_CHECK_RESULT(vkBindBufferMemory(m_device, m_geoIdxBuf,  m_geoMemAlloc, idxMemOffset));
//  m_pCopyHelper->UpdateBuffer(m_geoVertBuf, 0, triangleData,  sizeof(float) * 8 * triInfo.m_vertNum);
//  m_pCopyHelper->UpdateBuffer(m_geoIdxBuf,  0, indices, sizeof(uint32_t) * triInfo.m_indNum);
//
//  m_meshInfos.push_back(triInfo);
//
//  SetupPipeline();
//  UpdateView();
//
//  for(size_t i = 0; i < m_framesInFlight; ++i)
//  {
//    BuildCommandBuffer(m_cmdBuffers[i], m_frameBuffers[i], m_swapchain.GetAttachment(i).view);
//  }
//}

void RenderEngine::DrawFrameDeferred()
{
  vkWaitForFences(m_device, 1, &m_frameFences[m_presentationResources.currentFrame], VK_TRUE, UINT64_MAX);
  vkResetFences(m_device, 1, &m_frameFences[m_presentationResources.currentFrame]);

  uint32_t imageIdx;
  m_swapchain.AcquireNextImage(m_presentationResources.imageAvailable, &imageIdx);

  VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };

  BuildCommandBufferOffscreen(m_cmdBuffersDrawOffscreen[imageIdx], 0);
  BuildCommandBufferQuad( m_cmdBuffersDrawMain[imageIdx], m_frameBuffers[imageIdx]);

  VkSubmitInfo submitInfo = {};
  submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pWaitDstStageMask  = waitStages;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers    = &m_cmdBuffersDrawOffscreen[imageIdx];
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores    = &m_presentationResources.imageAvailable;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores    = &m_offscreenSemaphore;

  VK_CHECK_RESULT(vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));

  submitInfo.pWaitSemaphores    = &m_offscreenSemaphore;
  submitInfo.pCommandBuffers    = &m_cmdBuffersDrawMain[imageIdx];
  submitInfo.pSignalSemaphores  = &m_presentationResources.renderingFinished;
  VK_CHECK_RESULT(vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, m_frameFences[m_presentationResources.currentFrame]));

  VkResult presentRes = m_swapchain.QueuePresent(m_presentationResources.queue, imageIdx, m_presentationResources.renderingFinished);

  if (presentRes == VK_ERROR_OUT_OF_DATE_KHR || presentRes == VK_SUBOPTIMAL_KHR)
  {
    RecreateSwapChain();
  }
  else if (presentRes != VK_SUCCESS)
    {
      RUN_TIME_ERROR("Failed to present swapchain image");
    }

  m_presentationResources.currentFrame = (m_presentationResources.currentFrame + 1) % m_framesInFlight;

  vkQueueWaitIdle(m_presentationResources.queue);
}

void RenderEngine::DrawFrameSimple()
{
  vkWaitForFences(m_device, 1, &m_frameFences[m_presentationResources.currentFrame], VK_TRUE, UINT64_MAX);
  vkResetFences(m_device, 1, &m_frameFences[m_presentationResources.currentFrame]);

  uint32_t imageIdx;
  m_swapchain.AcquireNextImage(m_presentationResources.imageAvailable, &imageIdx);

  auto currentCmdBuf = m_renderSettings.wireframe_mode ? m_cmdBuffersDrawWire[imageIdx] : m_cmdBuffersDrawMain[imageIdx];

  VkSemaphore      waitSemaphores[] = { m_presentationResources.imageAvailable };
  VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };

  if(m_renderSettings.wireframe_mode)
  {
    BuildCommandBufferForward(currentCmdBuf, m_frameBuffers[imageIdx], m_pbrForwardPipeline, m_pbrForwardDS);
  }
  else
  {
    BuildCommandBufferForward(currentCmdBuf, m_frameBuffers[imageIdx], m_wireframePipeline, VK_NULL_HANDLE);
  }

  VkSubmitInfo submitInfo = {};
  submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores    = waitSemaphores;
  submitInfo.pWaitDstStageMask  = waitStages;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers    = &currentCmdBuf;

  VkSemaphore signalSemaphores[]  = { m_presentationResources.renderingFinished };
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores    = signalSemaphores;

  VK_CHECK_RESULT(vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, m_frameFences[m_presentationResources.currentFrame]));

  VkResult presentRes = m_swapchain.QueuePresent(m_presentationResources.queue, imageIdx, m_presentationResources.renderingFinished);

  if (presentRes == VK_ERROR_OUT_OF_DATE_KHR || presentRes == VK_SUBOPTIMAL_KHR)
  {
    RecreateSwapChain();
  }
  else if (presentRes != VK_SUCCESS)
    {
      RUN_TIME_ERROR("Failed to present swapchain image");
    }

  m_presentationResources.currentFrame = (m_presentationResources.currentFrame + 1) % m_framesInFlight;

  vkQueueWaitIdle(m_presentationResources.queue);
}

void RenderEngine::DrawFrame()
{
  if(m_renderSettings.gbuf_mode)
    DrawFrameDeferred();
  else
    DrawFrameSimple();
}

