#ifndef CHIMERA_HYBRID_RENDER_H
#define CHIMERA_HYBRID_RENDER_H

#define VK_NO_PROTOTYPES
#include "scene_mgr.h"
#include "render_common.h"
#include <geom/vk_mesh.h>
#include <vk_descriptor_sets.h>
#include <vk_fbuf_attachment.h>
#include <vk_images.h>
#include <vk_swapchain.h>
#include <string>
#include <iostream>

class RenderEngineHybrid : public IRender
{
public:
  RenderEngineHybrid(uint32_t a_width, uint32_t a_height);
  ~RenderEngineHybrid()  { Cleanup(); };

  inline uint32_t     GetWidth()      const override { return m_width; }
  inline uint32_t     GetHeight()     const override { return m_height; }
  inline VkInstance   GetVkInstance() const override { return m_instance; }
  void InitVulkan(std::vector<const char*> a_instanceExtensions, uint32_t a_deviceId) override;

  void InitPresentation(VkSurfaceKHR& a_surface) override;

  void ProcessInput(AppInput& input) override;
  void UpdateCamera(const Camera &cam) override;
  Camera GetCurrentCamera() override {return m_cam;}

  void UpdateView();

  void LoadScene(const std::string &path, bool transpose_inst_matrices) override;
  void DrawFrame() override;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // debugging utils
  //
  static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallbackFn(
    VkDebugReportFlagsEXT                       flags,
    VkDebugReportObjectTypeEXT                  objectType,
    uint64_t                                    object,
    size_t                                      location,
    int32_t                                     messageCode,
    const char* pLayerPrefix,
    const char* pMessage,
    void* pUserData)
  {
    std::cout << pLayerPrefix << " " << pMessage << std::endl;
    return VK_FALSE;
  }

  VkDebugReportCallbackEXT m_debugReportCallback = nullptr;

  static constexpr uint64_t scratchMemSize = 16 * 16 * 1024u;
private:


  struct RenderSettings
  {
    bool wireframe_mode = false;
  } m_renderSettings;

  VkInstance       m_instance       = VK_NULL_HANDLE;
  VkCommandPool    m_commandPool    = VK_NULL_HANDLE;
  VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
  VkDevice         m_device         = VK_NULL_HANDLE;
  VkQueue          m_graphicsQueue  = VK_NULL_HANDLE;
  VkQueue          m_transferQueue  = VK_NULL_HANDLE;
  VkQueue          m_computeQueue   = VK_NULL_HANDLE;

  vk_utils::QueueFID_T m_queueFamilyIDXs {UINT32_MAX, UINT32_MAX, UINT32_MAX};

  std::shared_ptr<vk_utils::ICopyEngine> m_pCopyHelper;

  struct
  {
    uint32_t    currentFrame      = 0u;
    VkQueue     queue             = VK_NULL_HANDLE;
    VkSemaphore imageAvailable    = VK_NULL_HANDLE;
    VkSemaphore renderingFinished = VK_NULL_HANDLE;
  } m_presentationResources;

  std::vector<VkFence> m_frameFences;
  std::vector<VkCommandBuffer> m_cmdBuffersDrawMain;
  std::vector<VkCommandBuffer> m_cmdBuffersDrawWire;

  VkSemaphore m_offscreenSemaphore = VK_NULL_HANDLE;
  PushConstMatrixViewID_T m_pushConsts;
  pipeline_data_t m_basicForwardPipeline {};
  pipeline_data_t m_wireframePipeline {};

  VkDescriptorSet       m_rtxDS = VK_NULL_HANDLE;;
  VkDescriptorSetLayout m_rtxDSLayout = VK_NULL_HANDLE;

  VkRenderPass m_screenRenderPass = VK_NULL_HANDLE; // main renderpass

  std::shared_ptr<vk_utils::DescriptorMaker> m_pBindings = nullptr;

  VkSurfaceKHR m_surface = VK_NULL_HANDLE;;
  VulkanSwapChain m_swapchain;
  std::vector<VkFramebuffer> m_frameBuffers;

  std::vector<VkFormat> m_depthFormats = {
    VK_FORMAT_D32_SFLOAT,
    VK_FORMAT_D32_SFLOAT_S8_UINT,
    VK_FORMAT_D24_UNORM_S8_UINT,
    VK_FORMAT_D16_UNORM_S8_UINT,
    VK_FORMAT_D16_UNORM
  };
  vk_utils::VulkanImageMem m_depthBuffer;

  Camera   m_cam;
  uint32_t m_width  = 1024u;
  uint32_t m_height = 1024u;
  uint32_t m_framesInFlight = 2u;
  bool m_vsync = false;

  VkPhysicalDeviceFeatures m_enabledDeviceFeatures = {};
  std::vector<const char*> m_deviceExtensions      = {};
  std::vector<const char*> m_instanceExtensions    = {};

  bool m_enableValidation;
  std::vector<const char*> m_validationLayers;

  std::shared_ptr<SceneManager> m_pScnMgr;

  void DrawFrameSimple();

  void CreateInstance();
  void CreateDevice(uint32_t a_deviceId);

  void BuildCommandBufferSimple(VkCommandBuffer cmdBuff, VkFramebuffer frameBuff, VkImageView a_targetImageView, VkPipeline a_pipeline);

  void SetupSimplePipeline();
  void CleanupPipelineAndSwapchain();
  void RecreateSwapChain();

  void Cleanup();

  void SetupDeviceFeatures();
  void SetupDeviceExtensions();
  void SetupValidationLayers();

  /// RTX
  VkPhysicalDeviceAccelerationStructureFeaturesKHR m_accelStructFeatures{};
  VkPhysicalDeviceAccelerationStructureFeaturesKHR m_enabledAccelStructFeatures{};
  VkPhysicalDeviceBufferDeviceAddressFeatures m_enabledDeviceAddressFeatures{};
  VkPhysicalDeviceRayQueryFeaturesKHR m_enabledRayQueryFeatures{};
  void * m_pDeviceFeatures = nullptr;
};


#endif//CHIMERA_HYBRID_RENDER_H
