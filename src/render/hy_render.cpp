#include "hy_render.h"
#include "../utils/input_definitions.h"

#include <geom/vk_mesh.h>
#include <vk_pipeline.h>
#include <ray_tracing/vk_rt_funcs.h>

RenderEngineHybrid::RenderEngineHybrid(uint32_t a_width, uint32_t a_height) :  m_width(a_width), m_height(a_height)
{
#ifdef NDEBUG
  m_enableValidation = false;
#else
  m_enableValidation = true;
#endif

}

void RenderEngineHybrid::SetupDeviceFeatures()
{
  m_enabledDeviceFeatures.fillModeNonSolid = VK_TRUE;
  m_enabledRayQueryFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
  m_enabledRayQueryFeatures.rayQuery = VK_TRUE;

  m_enabledDeviceAddressFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
  m_enabledDeviceAddressFeatures.bufferDeviceAddress = VK_TRUE;
  m_enabledDeviceAddressFeatures.pNext = &m_enabledRayQueryFeatures;

  m_enabledAccelStructFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
  m_enabledAccelStructFeatures.accelerationStructure = VK_TRUE;
  m_enabledAccelStructFeatures.pNext = &m_enabledDeviceAddressFeatures;

  m_pDeviceFeatures = &m_enabledAccelStructFeatures;

}

void RenderEngineHybrid::SetupDeviceExtensions()
{
  m_deviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

  m_deviceExtensions.push_back(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
  m_deviceExtensions.push_back(VK_KHR_RAY_QUERY_EXTENSION_NAME);

  // Required by VK_KHR_acceleration_structure
  m_deviceExtensions.push_back(VK_KHR_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME);
  m_deviceExtensions.push_back(VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME);
  m_deviceExtensions.push_back(VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME);

  // Required by VK_KHR_ray_tracing_pipeline
  m_deviceExtensions.push_back(VK_KHR_SPIRV_1_4_EXTENSION_NAME);

  // Required by VK_KHR_spirv_1_4
  m_deviceExtensions.push_back(VK_KHR_SHADER_FLOAT_CONTROLS_EXTENSION_NAME);
}

void RenderEngineHybrid::SetupValidationLayers()
{
  m_validationLayers.push_back("VK_LAYER_KHRONOS_validation");
  m_validationLayers.push_back("VK_LAYER_LUNARG_monitor");

  //useful layers: VK_LAYER_LUNARG_standard_validation, VK_LAYER_LUNARG_monitor, VK_LAYER_LUNARG_api_dump, VK_LAYER_KHRONOS_validation
}

void RenderEngineHybrid::InitVulkan(std::vector<const char*> a_instanceExtensions, uint32_t a_deviceId)
{
  m_instanceExtensions = std::move(a_instanceExtensions);
  SetupValidationLayers();
  VK_CHECK_RESULT(volkInitialize());
  CreateInstance();
  volkLoadInstance(m_instance);

  CreateDevice(a_deviceId);
  volkLoadDevice(m_device);

//  vk_rt_utils::LoadRayTracingFunctions(m_device);
  m_accelStructFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;

  VkPhysicalDeviceFeatures2 deviceFeatures2{};
  deviceFeatures2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
  deviceFeatures2.pNext = &m_accelStructFeatures;
  vkGetPhysicalDeviceFeatures2(m_physicalDevice, &deviceFeatures2);

  m_commandPool = vk_utils::createCommandPool(m_device, m_queueFamilyIDXs.graphics,
    VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

  m_cmdBuffersDrawMain.reserve(m_framesInFlight);
  m_cmdBuffersDrawMain = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);

  m_cmdBuffersDrawWire.reserve(m_framesInFlight);
  m_cmdBuffersDrawWire = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);

  m_frameFences.resize(m_framesInFlight);
  VkFenceCreateInfo fenceInfo = {};
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  for (size_t i = 0; i < m_framesInFlight; i++)
  {
    VK_CHECK_RESULT(vkCreateFence(m_device, &fenceInfo, nullptr, &m_frameFences[i]));
  }

  m_pCopyHelper = std::make_shared<vk_utils::PingPongCopyHelper>(m_physicalDevice, m_device, m_transferQueue,
    m_queueFamilyIDXs.transfer, scratchMemSize);

  LoaderConfig conf = {};
  conf.load_geometry = true;
  conf.load_materials = MATERIAL_LOAD_MODE::NONE;
  conf.build_acc_structs = true;
  conf.builder_type = BVH_BUILDER_TYPE::RTX;
  conf.instance_matrix_as_vertex_attribute = true;

  m_pScnMgr = std::make_shared<SceneManager>(m_device, m_physicalDevice, m_queueFamilyIDXs.graphics, m_pCopyHelper, conf);
}

void RenderEngineHybrid::InitPresentation(VkSurfaceKHR& a_surface)
{
  m_surface = a_surface;

  // presentation resources
  //
  m_presentationResources.queue = m_swapchain.CreateSwapChain(m_physicalDevice, m_device, m_surface, m_width, m_height,
           m_framesInFlight, m_vsync);
  m_presentationResources.currentFrame = 0;

  VkSemaphoreCreateInfo semaphoreInfo = {};
  semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
  VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_presentationResources.imageAvailable));
  VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_presentationResources.renderingFinished));
  VK_CHECK_RESULT(vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_offscreenSemaphore));
  m_screenRenderPass = vk_utils::createDefaultRenderPass(m_device, m_swapchain.GetFormat());

  vk_utils::getSupportedDepthFormat(m_physicalDevice, m_depthFormats, &m_depthBuffer.format);
  m_depthBuffer = vk_utils::createDepthTexture(m_device, m_physicalDevice, m_width, m_height, m_depthBuffer.format);

  m_frameBuffers = vk_utils::createFrameBuffers(m_device, m_swapchain, m_screenRenderPass, m_depthBuffer.view);

}

void RenderEngineHybrid::CreateInstance()
{
  VkApplicationInfo appInfo = {};
  appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pNext              = nullptr;
  appInfo.pApplicationName   = "Render";
  appInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
  appInfo.pEngineName        = "Chimera";
  appInfo.engineVersion      = VK_MAKE_VERSION(0, 1, 0);
  appInfo.apiVersion         = VK_MAKE_VERSION(1, 2, 0);

  m_instance = vk_utils::createInstance(m_enableValidation, m_validationLayers, m_instanceExtensions, &appInfo);

  if (m_enableValidation)
    vk_utils::initDebugReportCallback(m_instance, &debugReportCallbackFn, &m_debugReportCallback);
}

void RenderEngineHybrid::CreateDevice(uint32_t a_deviceId)
{
  SetupDeviceExtensions();
  m_physicalDevice = vk_utils::findPhysicalDevice(m_instance, true, a_deviceId, m_deviceExtensions);

  SetupDeviceFeatures();
  m_device = vk_utils::createLogicalDevice(m_physicalDevice, m_validationLayers, m_deviceExtensions,
    m_enabledDeviceFeatures, m_queueFamilyIDXs, VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT | VK_QUEUE_COMPUTE_BIT,
                                           m_pDeviceFeatures);

  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.graphics, 0, &m_graphicsQueue);
  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.transfer, 0, &m_transferQueue);
  vkGetDeviceQueue(m_device, m_queueFamilyIDXs.compute,  0, &m_computeQueue);
}


void RenderEngineHybrid::SetupSimplePipeline()
{
  std::vector<std::pair<VkDescriptorType, uint32_t> > dtypes = {
    {VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, 1},
    {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1}
  };
  m_pBindings = std::make_shared<vk_utils::DescriptorMaker>(m_device, dtypes, 1);

  m_pBindings->BindBegin(VK_SHADER_STAGE_FRAGMENT_BIT);
  m_pBindings->BindAccelStruct(0, m_pScnMgr->GetTLAS());
  m_pBindings->BindEnd(&m_rtxDS, &m_rtxDSLayout);

  vk_utils::GraphicsPipelineMaker maker;

  std::unordered_map<VkShaderStageFlagBits, std::string> shader_paths;
  shader_paths[VK_SHADER_STAGE_FRAGMENT_BIT] = "../resources/shaders/rayquery.frag.spv";
  shader_paths[VK_SHADER_STAGE_VERTEX_BIT] = "../resources/shaders/simple.vert.spv";

  maker.LoadShaders(m_device, shader_paths);

  m_basicForwardPipeline.layout = maker.MakeLayout(m_device, {m_rtxDSLayout}, sizeof(m_pushConsts));
  maker.SetDefaultState(m_width, m_height);

  maker.pipelineInfo.flags              = VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;
  maker.pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
  maker.pipelineInfo.basePipelineIndex  = -1;
  m_basicForwardPipeline.pipeline = maker.MakePipeline(m_device, m_pScnMgr->GetPipelineVertexInputStateCreateInfo(), m_screenRenderPass);

  // wireframe ***********************
  shader_paths[VK_SHADER_STAGE_FRAGMENT_BIT] = "../resources/shaders/wireframe.frag.spv";
  shader_paths[VK_SHADER_STAGE_VERTEX_BIT] = "../resources/shaders/wireframe.vert.spv";

  maker.LoadShaders(m_device, shader_paths);
  m_wireframePipeline.layout = maker.MakeLayout(m_device, {m_rtxDSLayout}, sizeof(m_pushConsts));
  maker.SetDefaultState(m_width, m_height);
  // setup derivative pipeline differences
  {
    maker.pipelineInfo.flags              = VK_PIPELINE_CREATE_DERIVATIVE_BIT;
    maker.pipelineInfo.basePipelineHandle = m_basicForwardPipeline.pipeline;
    maker.pipelineInfo.basePipelineIndex  = -1;
    maker.rasterizer.polygonMode = VK_POLYGON_MODE_LINE;
    maker.rasterizer.cullMode = VK_CULL_MODE_NONE;
    maker.rasterizer.lineWidth = 1.0f;
    maker.colorBlendAttachments[0].blendEnable = VK_FALSE;
  }

  m_wireframePipeline.pipeline = maker.MakePipeline(m_device, m_pScnMgr->GetPipelineVertexInputStateCreateInfo(), m_screenRenderPass);
}

void RenderEngineHybrid::BuildCommandBufferSimple(VkCommandBuffer a_cmdBuff, VkFramebuffer a_frameBuff, VkImageView a_targetImageView, VkPipeline a_pipeline)
{
  vkResetCommandBuffer(a_cmdBuff, 0);

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

  VK_CHECK_RESULT(vkBeginCommandBuffer(a_cmdBuff, &beginInfo));

  vk_utils::setDefaultViewport(a_cmdBuff, static_cast<float>(m_width), static_cast<float>(m_height));
  vk_utils::setDefaultScissor(a_cmdBuff, m_width, m_height);

  ///// draw final scene to screen
  {
    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType             = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass        = m_screenRenderPass;
    renderPassInfo.framebuffer       = a_frameBuff;
    renderPassInfo.renderArea.offset = { 0, 0 };
    renderPassInfo.renderArea.extent = m_swapchain.GetExtent();

    VkClearValue clearValues[2] = {};
    clearValues[0].color           = {0.0f, 0.0f, 0.0f, 1.0f};
    clearValues[1].depthStencil    = {1.0f, 0};
    renderPassInfo.clearValueCount = 2;
    renderPassInfo.pClearValues    = &clearValues[0];

    vkCmdBeginRenderPass(a_cmdBuff, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline   (a_cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, a_pipeline);

    vkCmdBindDescriptorSets(a_cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, m_basicForwardPipeline.layout, 0, 1,
                            &m_rtxDS, 0, NULL);

    VkShaderStageFlags stageFlags = (VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);

    VkDeviceSize zero_offset = 0u;
    VkBuffer vertexBuf = m_pScnMgr->GetVertexBuffer();
    VkBuffer indexBuf  = m_pScnMgr->GetIndexBuffer();
    VkBuffer instBuf   = m_pScnMgr->GetInstanceMatBuffer();

    vkCmdBindVertexBuffers(a_cmdBuff, 0, 1, &vertexBuf, &zero_offset);
    vkCmdBindIndexBuffer  (a_cmdBuff, indexBuf, 0, VK_INDEX_TYPE_UINT32);
    vkCmdBindVertexBuffers(a_cmdBuff, 1, 1, &instBuf, &zero_offset);

    for(size_t i = 0; i < m_pScnMgr->InstancesNum(); ++i)
    {
      auto inst = m_pScnMgr->GetInstanceInfo(i);

      m_pushConsts.instanceID = i;
      vkCmdPushConstants(a_cmdBuff, m_basicForwardPipeline.layout, stageFlags, 0,
                         sizeof(m_pushConsts), &m_pushConsts);

      auto mesh_info = m_pScnMgr->GetMeshInfo(inst.mesh_id);
      vkCmdDrawIndexed(a_cmdBuff, mesh_info.m_indNum, 1, mesh_info.m_indexOffset, mesh_info.m_vertexOffset, 0);
    }

    vkCmdEndRenderPass(a_cmdBuff);
  }

  VK_CHECK_RESULT(vkEndCommandBuffer(a_cmdBuff));
}


void RenderEngineHybrid::CleanupPipelineAndSwapchain()
{
  if(!m_cmdBuffersDrawMain.empty())
  {
    vkFreeCommandBuffers(m_device, m_commandPool, static_cast<uint32_t>(m_cmdBuffersDrawMain.size()), m_cmdBuffersDrawMain.data());
    m_cmdBuffersDrawMain.clear();
  }
  if(!m_cmdBuffersDrawWire.empty())
  {
    vkFreeCommandBuffers(m_device, m_commandPool, static_cast<uint32_t>(m_cmdBuffersDrawWire.size()), m_cmdBuffersDrawWire.data());
    m_cmdBuffersDrawWire.clear();
  }

  for (size_t i = 0; i < m_frameFences.size(); i++)
  {
    vkDestroyFence(m_device, m_frameFences[i], nullptr);
  }

  vkDestroyImageView(m_device, m_depthBuffer.view, nullptr);
  vkDestroyImage(m_device, m_depthBuffer.image, nullptr);

  for (size_t i = 0; i < m_frameBuffers.size(); i++)
  {
    vkDestroyFramebuffer(m_device, m_frameBuffers[i], nullptr);
  }

  vkDestroyRenderPass(m_device, m_screenRenderPass, nullptr);

  m_swapchain.Cleanup();
}

void RenderEngineHybrid::RecreateSwapChain()
{
  vkDeviceWaitIdle(m_device);

  CleanupPipelineAndSwapchain();
  m_presentationResources.queue = m_swapchain.CreateSwapChain(m_physicalDevice, m_device, m_surface, m_width, m_height,
    m_framesInFlight, m_vsync);

  m_screenRenderPass = vk_utils::createDefaultRenderPass(m_device, m_swapchain.GetFormat());

  vk_utils::getSupportedDepthFormat(m_physicalDevice, m_depthFormats, &m_depthBuffer.format);
  m_depthBuffer = vk_utils::createDepthTexture(m_device, m_physicalDevice, m_width, m_height, m_depthBuffer.format);

  m_frameBuffers = vk_utils::createFrameBuffers(m_device, m_swapchain, m_screenRenderPass, m_depthBuffer.view);

  m_frameFences.resize(m_framesInFlight);
  VkFenceCreateInfo fenceInfo = {};
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  for (size_t i = 0; i < m_framesInFlight; i++)
  {
    VK_CHECK_RESULT(vkCreateFence(m_device, &fenceInfo, nullptr, &m_frameFences[i]));
  }

  m_cmdBuffersDrawMain = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);
  m_cmdBuffersDrawWire = vk_utils::createCommandBuffers(m_device, m_commandPool, m_framesInFlight);
  for(size_t i = 0; i < m_swapchain.GetImageCount(); ++i)
  {
    BuildCommandBufferSimple(m_cmdBuffersDrawMain[i], m_frameBuffers[i],
      m_swapchain.GetAttachment(i).view, m_basicForwardPipeline.pipeline);

    BuildCommandBufferSimple(m_cmdBuffersDrawWire[i], m_frameBuffers[i],
      m_swapchain.GetAttachment(i).view, m_wireframePipeline.pipeline);

  }

}

void RenderEngineHybrid::Cleanup()
{
  CleanupPipelineAndSwapchain();

  if(m_offscreenSemaphore != VK_NULL_HANDLE)
  {
    vkDestroySemaphore(m_device, m_offscreenSemaphore, nullptr);
  }

  if(m_wireframePipeline.pipeline != VK_NULL_HANDLE)
  {
    vkDestroyPipeline(m_device, m_wireframePipeline.pipeline, nullptr);
  }
  if(m_wireframePipeline.layout != VK_NULL_HANDLE)
  {
    vkDestroyPipelineLayout(m_device, m_wireframePipeline.layout, nullptr);
  }

  if(m_basicForwardPipeline.pipeline != VK_NULL_HANDLE)
  {
    vkDestroyPipeline(m_device, m_basicForwardPipeline.pipeline, nullptr);
  }
  if(m_basicForwardPipeline.layout != VK_NULL_HANDLE)
  {
    vkDestroyPipelineLayout(m_device, m_basicForwardPipeline.layout, nullptr);
  }

  if(m_presentationResources.imageAvailable != VK_NULL_HANDLE)
  {
    vkDestroySemaphore(m_device, m_presentationResources.imageAvailable, nullptr);
  }
  if(m_presentationResources.renderingFinished != VK_NULL_HANDLE)
  {
    vkDestroySemaphore(m_device, m_presentationResources.renderingFinished, nullptr);
  }

  if(m_commandPool != VK_NULL_HANDLE)
  {
    vkDestroyCommandPool(m_device, m_commandPool, nullptr);
  }
}

void RenderEngineHybrid::ProcessInput(AppInput& input)
{
  if(input.keyPressed[GLFW_KEY_SPACE])
    m_renderSettings.wireframe_mode = !m_renderSettings.wireframe_mode;
}

void RenderEngineHybrid::UpdateCamera(const Camera &cam)
{
  m_cam = cam;
  UpdateView();
}

void RenderEngineHybrid::UpdateView()
{
  const float aspect = float(m_width) / float(m_height);
  auto mProjFix = OpenglToVulkanProjectionMatrixFix();
  auto mProj    = projectionMatrix(m_cam.fov, aspect, 0.1f, 1000.0f);
  auto mLookAt  = LiteMath::lookAt(m_cam.pos, m_cam.lookAt, m_cam.up);
  auto mWorldViewProj = mProjFix * mProj * mLookAt;

  m_pushConsts.viewPos   = m_cam.pos;
  m_pushConsts.mProjView = mWorldViewProj;
}

void RenderEngineHybrid::LoadScene(const std::string &path, bool transpose_inst_matrices)
{
//  m_pScnMgr->LoadSceneXML(path, transpose_inst_matrices);
//
//  m_pScnMgr->BuildAllBLAS();
//  m_pScnMgr->BuildTLAS();

  m_pScnMgr->LoadScene(path);
  m_pScnMgr->BuildTLAS();

  SetupSimplePipeline();

  auto loadedCam = m_pScnMgr->GetCamera(0);
  m_cam.fov = loadedCam.fov;
  m_cam.pos = float3(loadedCam.pos);
  m_cam.up  = float3(loadedCam.up);
  m_cam.lookAt = float3(loadedCam.lookAt);
  m_cam.tdist  = loadedCam.farPlane;
  UpdateView();

  for(size_t i = 0; i < m_framesInFlight; ++i)
  {
    BuildCommandBufferSimple(m_cmdBuffersDrawMain[i], m_frameBuffers[i],
      m_swapchain.GetAttachment(i).view, m_basicForwardPipeline.pipeline);

    BuildCommandBufferSimple(m_cmdBuffersDrawWire[i], m_frameBuffers[i],
      m_swapchain.GetAttachment(i).view, m_wireframePipeline.pipeline);
  }
}

void RenderEngineHybrid::DrawFrameSimple()
{
  vkWaitForFences(m_device, 1, &m_frameFences[m_presentationResources.currentFrame], VK_TRUE, UINT64_MAX);
  vkResetFences(m_device, 1, &m_frameFences[m_presentationResources.currentFrame]);

  uint32_t imageIdx;
  m_swapchain.AcquireNextImage(m_presentationResources.imageAvailable, &imageIdx);

  auto currentCmdBuf = m_renderSettings.wireframe_mode ? m_cmdBuffersDrawWire[imageIdx] : m_cmdBuffersDrawMain[imageIdx];

  VkSemaphore      waitSemaphores[] = { m_presentationResources.imageAvailable };
  VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };

  if(m_renderSettings.wireframe_mode)
  {
    BuildCommandBufferSimple(currentCmdBuf, m_frameBuffers[imageIdx], m_swapchain.GetAttachment(imageIdx).view, m_wireframePipeline.pipeline);
  }
  else
  {
    BuildCommandBufferSimple(currentCmdBuf, m_frameBuffers[imageIdx], m_swapchain.GetAttachment(imageIdx).view, m_basicForwardPipeline.pipeline);
  }

  VkSubmitInfo submitInfo = {};
  submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores    = waitSemaphores;
  submitInfo.pWaitDstStageMask  = waitStages;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers    = &currentCmdBuf;

  VkSemaphore signalSemaphores[]  = { m_presentationResources.renderingFinished };
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores    = signalSemaphores;

  VK_CHECK_RESULT(vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, m_frameFences[m_presentationResources.currentFrame]));

  VkResult presentRes = m_swapchain.QueuePresent(m_presentationResources.queue, imageIdx, m_presentationResources.renderingFinished);

  if (presentRes == VK_ERROR_OUT_OF_DATE_KHR || presentRes == VK_SUBOPTIMAL_KHR)
  {
    RecreateSwapChain();
  }
  else if (presentRes != VK_SUCCESS)
  {
    RUN_TIME_ERROR("Failed to present swapchain image");
  }

  m_presentationResources.currentFrame = (m_presentationResources.currentFrame + 1) % m_framesInFlight;

  vkQueueWaitIdle(m_presentationResources.queue);
}

void RenderEngineHybrid::DrawFrame()
{
  DrawFrameSimple();
}

