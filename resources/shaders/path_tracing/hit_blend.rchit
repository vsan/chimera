#version 460
#extension GL_EXT_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : enable
#extension GL_GOOGLE_include_directive : require

#include "../common.h"
#include "../crandom.h"
#include "../materialsCommon.h"

layout(location = 0) rayPayloadInEXT RayPayload_V2 hitValue;

hitAttributeEXT vec3 attribs;

layout(binding = 0, set = 0) uniform accelerationStructureEXT topLevelAS;
layout(binding = 2, set = 0) uniform AppData
{
	UniformParams Params;
};

layout(binding = 3, set = 0) buffer Vertices { vec4 v[]; } vertices;
layout(binding = 4, set = 0) buffer Indices { uint i[]; } indices;
layout(binding = 5, set = 0, rg32ui) uniform uimage2D rndStateImage;
layout(binding = 6, set = 0) buffer MeshInfos { uvec2 o[]; } infos;


Vertex unpack(uint index, uint vertex_sz)
{
	const uint m = vertex_sz / 16;

	vec4 d0 = vertices.v[m * index + 0];
	vec4 d1 = vertices.v[m * index + 1];

	Vertex v;
	v.pos = d0.xyz;
	v.normal = DecodeNormal(floatBitsToInt(d0.w));
	v.uv = d1.st;

	return v;
}

void main()
{
	uint idxOffset = infos.o[gl_InstanceCustomIndexEXT].x;
	uint vtxOffset = infos.o[gl_InstanceCustomIndexEXT].y;

	ivec3 index = ivec3(indices.i[idxOffset + gl_PrimitiveID * 3 + 0],
	indices.i[idxOffset + gl_PrimitiveID * 3 + 1],
	indices.i[idxOffset + gl_PrimitiveID * 3 + 2]);

	Vertex v0 = unpack(index.x + vtxOffset, Params.vertexSize);
	Vertex v1 = unpack(index.y + vtxOffset, Params.vertexSize);
	Vertex v2 = unpack(index.z + vtxOffset, Params.vertexSize);

	const vec3 barycentricCoords = vec3(1.0f - attribs.x - attribs.y, attribs.x, attribs.y);
	const vec3 N = normalize(v0.normal * barycentricCoords.x + v1.normal * barycentricCoords.y + v2.normal * barycentricCoords.z);
	vec3 normal = normalize(gl_ObjectToWorldEXT * vec4(N, 0.0f));
	const float flipNorm = dot(gl_WorldRayDirectionEXT, normal) > 0.001f ? -1.0f : 1.0f;
	normal *= flipNorm;

	const vec3 hitPos = gl_WorldRayOriginEXT + gl_WorldRayDirectionEXT * gl_HitTEXT;

	const vec3 obj_color = vec3(1.0f, 0.76f, 0.79f);
	uvec2 genState = imageLoad(rndStateImage, ivec2(gl_LaunchIDEXT.xy)).rg;

	hitValue.ray_pos.xyz = gl_WorldRayOriginEXT;
	hitValue.ray_dir.xyz = gl_WorldRayDirectionEXT;
	hitValue.colorAndDist.xyz = BlendLambertGGX(hitPos, normal, rndFloat4(genState), obj_color, 0.25f, hitValue.ray_pos, hitValue.ray_dir);
	hitValue.colorAndDist.w   = gl_HitTEXT;

	imageStore(rndStateImage, ivec2(gl_LaunchIDEXT.xy), uvec4(genState, 0, 0));
}
