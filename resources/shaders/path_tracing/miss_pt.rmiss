#version 460
#extension GL_EXT_ray_tracing : require
#extension GL_GOOGLE_include_directive : require

#include "../common.h"

layout(location = 0) rayPayloadInEXT RayPayload hitValue;
//layout(location = 0) rayPayloadInEXT vec3 hitValue;

void main()
{
//    hitValue = vec3(0.0, 0.0, 0.2);
    hitValue.normalAndDist = vec4(0.0f, 0.0f, 0.2f, -1.0f);
    hitValue.baryMatID     = vec4(0.0f);
}