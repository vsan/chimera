#ifndef CHIMERA_CRANDOM_H
#define CHIMERA_CRANDOM_H

#ifdef __cplusplus
#include "LiteMath.h"
using LiteMath::uint2;
using LiteMath::float2;
using LiteMath::float4;
using LiteMath::make_float2;
using LiteMath::make_float4;

typedef unsigned int uint;
typedef uint2        uvec2;
typedef float4       vec4;
typedef float2       vec2;

uint NextState(uvec2& state)
{
  const uint x = state.x * 17 + state.y * 13123;
  state.x = (x << 13) ^ x;
  state.y ^= (x << 7);
  return x;
}

#else

uint NextState(inout uvec2 state)
{
  const uint x = state.x * 17 + state.y * 13123;
  state.x = (x << 13) ^ x;
  state.y ^= (x << 7);
  return x;
}

vec4 rndFloat4(inout uvec2 state)
{
  const uint x = NextState(state);

  const uint x1 = (x * (x * x * 15731 + 74323) + 871483);
  const uint y1 = (x * (x * x * 13734 + 37828) + 234234);
  const uint z1 = (x * (x * x * 11687 + 26461) + 137589);
  const uint w1 = (x * (x * x * 15707 + 789221) + 1376312589);

  const float scale = (1.0f / 4294967296.0f);

  return vec4(float(x1), float(y1), float(z1), float(w1))*scale;
}

vec2 rndFloat2(inout uvec2 state)
{
  const uint x = NextState(state);

  const uint x1 = (x * (x * x * 15731 + 74323) + 871483);
  const uint y1 = (x * (x * x * 13734 + 37828) + 234234);
  const float scale = (1.0f / 4294967296.0f);

  return vec2(float(x1), float(y1))*scale;
}

float rndFloat(inout uvec2 state)
{
  const uint x = NextState(state);
  const uint x1 = (x * (x * x * 15731 + 74323) + 871483);
  const float scale = (1.0f / 4294967296.0f);
  return float(x1)*scale;
}
#endif

uvec2 RandomGenInit(const int a_seed)
{
  uvec2 state;

  state.x  = (a_seed * (a_seed * a_seed * 15731 + 74323) + 871483);
  state.y  = (a_seed * (a_seed * a_seed * 13734 + 37828) + 234234);

  for(int i=0;i<(a_seed%7);i++)
    NextState(state);

  return state;
}


/**
\brief  transform float2 sample in rect [-1,1]x[-1,1] to disc centered at (0,0) with radius == 1.
\param  xy - input sample in rect [-1,1]x[-1,1]
\return position in disc

*/
vec2 MapSamplesToDisc(vec2 xy)
{
  float x = xy.x;
  float y = xy.y;

  float r = 0;
  float phi = 0;

  vec2 res = xy;

  if (x > y && x > -y)
  {
    r = x;
    phi = 0.25f*3.141592654f*(y / x);
  }

  if (x < y && x > -y)
  {
    r = y;
    phi = 0.25f*3.141592654f*(2.0f - x / y);
  }

  if (x < y && x < -y)
  {
    r = -x;
    phi = 0.25f*3.141592654f*(4.0f + y / x);
  }

  if (x >y && x<-y)
  {
    r = -y;
    phi = 0.25f*3.141592654f*(6 - x / y);
  }

  //float sin_phi, cos_phi;
  //sincosf(phi, &sin_phi, &cos_phi);
  float sin_phi = sin(phi);
  float cos_phi = cos(phi);

  res.x = r*sin_phi;
  res.y = r*cos_phi;

  return res;
}

#endif//CHIMERA_CRANDOM_H
