#version 460
#extension GL_EXT_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : enable
#extension GL_GOOGLE_include_directive : require

#include "unpack_attributes.h"
#include "common.h"

layout(location = 0) rayPayloadInEXT vec3 hitValue;
hitAttributeEXT vec3 attribs;

layout(binding = 0, set = 0) uniform accelerationStructureEXT topLevelAS;
layout(binding = 2, set = 0) uniform AppData
{
	UniformParams Params;
};

layout(binding = 3, set = 0) buffer Vertices { vec4 v[]; } vertices;
layout(binding = 4, set = 0) buffer Indices { uint i[]; } indices;
layout(binding = 5, set = 0) buffer MeshInfos { uvec2 o[]; } infos;

struct Vertex
{
	vec3 pos;
	vec3 normal;
	vec2 uv;
};

Vertex unpackVertex(uint index)
{
	const uint m = Params.vertexSize / 16;

	vec4 d0 = vertices.v[m * index + 0];
	vec4 d1 = vertices.v[m * index + 1];

	Vertex v;
	v.pos = d0.xyz;
	v.normal = DecodeNormal(floatBitsToInt(d0.w));
	v.uv = d1.st;

	return v;
}

void main()
{
	uint idxOffset = infos.o[gl_InstanceCustomIndexEXT].x;
	uint vtxOffset = infos.o[gl_InstanceCustomIndexEXT].y;

	ivec3 index = ivec3(indices.i[idxOffset + gl_PrimitiveID * 3 + 0],
											indices.i[idxOffset + gl_PrimitiveID * 3 + 1],
											indices.i[idxOffset + gl_PrimitiveID * 3 + 2]);

	Vertex v0 = unpackVertex(index.x + vtxOffset);
	Vertex v1 = unpackVertex(index.y + vtxOffset);
	Vertex v2 = unpackVertex(index.z + vtxOffset);

	// Interpolate normal
	const vec3 barycentricCoords = vec3(1.0f - attribs.x - attribs.y, attribs.x, attribs.y);
	vec3 normal = normalize(v0.normal * barycentricCoords.x + v1.normal * barycentricCoords.y + v2.normal * barycentricCoords.z);

	// Basic lighting
	vec3 lightVector = vec3(1.0, 1.0, 0.0);//normalize(ubo.lightPos.xyz);
	float dot_product = max(dot(lightVector, normal), 0.2);
	hitValue = vec3(0.0, 1.0, 0.0) * dot_product;
}
