#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : require

#include "unpack_attributes.h"
#include "common.h"

layout(location = 0) in vec4 vPosNorm;
layout(location = 1) in vec4 vTexCoordAndTang;
layout(location = 2) in mat4x4 instanceMatrix;

layout(push_constant) uniform pushConst
{
    PushConstMatrixViewID_T params;
};


layout (location = 0 ) out VS_OUT
{
    vec3 wPos;
    vec3 wNorm;
    vec3 wTangent;
    vec2 texCoord;
} vOut;

out gl_PerVertex { vec4 gl_Position; };
void main(void)
{
//    const mat4x4 instanceMatrix = params.mModel;

    const vec3 wNorm = DecodeNormal(floatBitsToUint(vPosNorm.w));
    const vec3 wTang = DecodeNormal(floatBitsToUint(vTexCoordAndTang.z));

    vOut.wPos     = (instanceMatrix * vec4(vPosNorm.xyz, 1.0f)).xyz;
    vOut.wNorm    = normalize(mat3(transpose(inverse(instanceMatrix))) * wNorm);
    vOut.wTangent = normalize(mat3(transpose(inverse(instanceMatrix))) * wTang);
    vOut.texCoord = vTexCoordAndTang.xy;

    gl_Position   = params.mProjView * vec4(vOut.wPos, 1.0);
}
