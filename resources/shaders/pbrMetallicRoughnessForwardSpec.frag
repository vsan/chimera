#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_nonuniform_qualifier : enable
#extension GL_GOOGLE_include_directive : require

#include "common.h"

layout(location = 0) out vec4 out_fragColor;

layout (location = 0 ) in VS_OUT
{
    vec3 wPos;
    vec3 wNorm;
    vec3 wTangent;
    vec2 texCoord;
} surf;

layout(push_constant) uniform pushConst
{
    PushConstMatrixViewID_T params;
};

layout (binding = 0, set = 0, std430) buffer Materials { MaterialData_pbrMR materials[]; };
layout (binding = 1, set = 0, std430) buffer MaterialsID { uint matID[]; };
layout (binding = 2, set = 0) uniform sampler2D all_textures[MAX_TEXTURES];
layout (binding = 3, set = 0) buffer MeshInfos { uvec2 o[]; } infos;


// Normal Distribution function --------------------------------------
float D_GGX(float dotNH, float roughness)
{
    float alpha = roughness * roughness;
    float alpha2 = alpha * alpha;
    float denom = dotNH * dotNH * (alpha2 - 1.0) + 1.0;
    return (alpha2)/(PI * denom*denom);
}

// Geometric Shadowing function --------------------------------------
float G_SchlicksmithGGX(float dotNL, float dotNV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;
    float GL = dotNL / (dotNL * (1.0 - k) + k);
    float GV = dotNV / (dotNV * (1.0 - k) + k);
    return GL * GV;
}

// Fresnel function ----------------------------------------------------
vec3 F_Schlick(float cosTheta, float metallic, vec3 base_color)
{
    vec3 F0 = mix(vec3(0.04), base_color, metallic); // * material.specular
    vec3 F = F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
    return F;
}

// Specular BRDF composition --------------------------------------------
vec3 GGX_Reflection(vec3 L, vec3 V, vec3 N, float metallic, float roughness, vec3 base_color)
{
    // Precalculate vectors and dot products
    vec3 H = normalize (V + L);
    float dotNV = clamp(dot(N, V), 0.0, 1.0);
    float dotNL = clamp(dot(N, L), 0.0, 1.0);
    float dotLH = clamp(dot(L, H), 0.0, 1.0);
    float dotNH = clamp(dot(N, H), 0.0, 1.0);

    vec3 color = vec3(0.0);
    if (dotNL > 0.0)
    {
        float rroughness = max(0.05, roughness);
        float D = D_GGX(dotNH, roughness);
        float G = G_SchlicksmithGGX(dotNL, dotNV, roughness);
        vec3 F = F_Schlick(dotNV, metallic, base_color);

        vec3 spec = D * F * G / (4.0 * dotNL * dotNV);
        color += spec * dotNL;
    }

    return color;
}

float lightDiffuseContrib(vec3 L, vec3 N, float ambient)
{
    return max(dot(N, L), ambient);
}

void main()
{
    const uint offset = infos.o[params.instanceID].x;
    const uint matIdx = matID[gl_PrimitiveID + offset / 3];
    vec4 base_color = materials[matIdx].baseColor;
    if(materials[matIdx].baseColorTexId >= 0)
    {
        const vec4 tex_val = texture(all_textures[materials[matIdx].baseColorTexId], surf.texCoord);
        base_color.xyz *= tex_val.xyz;
        base_color.a    = tex_val.a;
    }

    if (materials[matIdx].alphaMode == 1)
    {
        if (base_color.a < materials[matIdx].alphaCutoff)
        {
            discard;
        }
    }

    const vec3 emissionColor = materials[matIdx].emissionColor * texture(all_textures[materials[matIdx].emissionTexId], surf.texCoord).rgb;

    vec3 lightDir1 = vec3(1.0f, 1.0f, 1.0f);
    vec3 lightDir2 = vec3(-1.0f, 1.0f, 1.0f);
    vec3 lightDir3 = vec3(0.0f, 1.0f, 1.0f);

    vec3 lightColor1 = vec3(0.64f, 1.0f, 1.0f);
    vec3 lightColor2 = vec3(1.0f, 0.56f, 0.001f);
    vec3 lightColor3 = vec3(1.0f, 1.0f, 1.0f);

    //    vec3 lightColor1 = vec3(1.0f, 0.0f, 0.0f);
    //    vec3 lightColor2 = vec3(0.0f, 1.0f, 0.0f);
    //    vec3 lightColor3 = vec3(1.0f, 1.0f, 1.0f);

    vec3 N = surf.wNorm; //dot(surf.wNorm, (params.wCamPos.xyz - surf.wPos)) > 0 ? surf.wNorm : -surf.wNorm;
    if(materials[matIdx].normalTexId >= 0)
    {
        vec3 T = surf.wTangent;
        vec3 B = normalize(cross(N, T));
        mat3 TBN = mat3(T, B, N);
        N = TBN * normalize(texture(all_textures[materials[matIdx].normalTexId], surf.texCoord).xyz * 2.0 - vec3(1.0));
    }

    const vec3 V = normalize(params.viewPos - surf.wPos);
    const float metallic  = materials[matIdx].metallic * texture(all_textures[materials[matIdx].metallicRoughnessTexId], surf.texCoord).b;
    const float roughness = materials[matIdx].roughness * texture(all_textures[materials[matIdx].metallicRoughnessTexId], surf.texCoord).g;

    vec3 Lo = emissionColor;

    vec3 L = normalize(lightDir1 - surf.wPos);
    Lo += GGX_Reflection(L, V, N, metallic, roughness, base_color.xyz) * lightColor1;
    Lo += lightDiffuseContrib(L, N, 0.05f) * base_color.xyz * lightColor1 * 0.45f;
    L = normalize(lightDir2 - surf.wPos);
    Lo += GGX_Reflection(L, V, N, metallic, roughness, base_color.xyz) * lightColor2;
    Lo += lightDiffuseContrib(L, N, 0.05f) * base_color.xyz * lightColor2 * 0.45f;
    L = normalize(lightDir3 - surf.wPos);
    Lo += GGX_Reflection(L, V, N, metallic, roughness, base_color.xyz) * lightColor3;
    Lo += lightDiffuseContrib(L, N, 0.05f) * base_color.xyz * lightColor3 * 0.1f;

//    const uint palette_size = 20;
//    const vec4 m_palette[20] = {
//        vec4(1.0, 0.0, 0.0, 1.0), vec4(1.0, 1.0, 0.0, 1.0), vec4(1.0, 0.0, 1.0, 1.0),
//        vec4(0.0, 1.0, 0.0, 1.0), vec4(0.0, 1.0, 1.0, 1.0), vec4(0.0, 0.0, 1.0, 1.0),
//        vec4(1.0, 1.0, 1.0, 1.0), vec4(0.75, 0.25, 0.0, 1.0), vec4(1.75, 0.0, 1.25, 1.0),
//        vec4(0.0, 0.75, 0.25, 1.0), vec4(0.75, 0.25, 0.25, 1.0), vec4(0.25, 0.75, 0.25, 1.0),
//        vec4(0.25, 0.25, 0.75, 1.0), vec4(0.9, 0.5, 0.0, 1.0), vec4(0.5, 0.9, 0.0, 1.0),
//        vec4(0.9, 0.0, 0.5, 1.0), vec4(0.5, 0.0, 0.9, 1.0), vec4(0.0, 0.5, 0.9, 1.0),
//        vec4(0.5, 0.9, 0.5, 1.0), vec4(0.65, 0.2, 0.4, 1.0)
//    };

    out_fragColor = vec4(Lo, 1.0f);
//    out_fragColor = m_palette[matIdx % palette_size];
}
