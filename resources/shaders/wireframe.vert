#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : require

#include "common.h"

layout(location = 0) in vec4 vPosNorm;
layout(location = 1) in vec4 vTexCoordAndTang;
layout(location = 2) in mat4x4 instanceMatrix;

layout(push_constant) uniform pushConst
{
  PushConstMatrixViewID_T params;
};


out gl_PerVertex { vec4 gl_Position; };

void main()
{
//        const mat4x4 instanceMatrix = params.mModel;
    gl_Position   = params.mProjView * instanceMatrix * vec4(vPosNorm.xyz, 1.0);
}