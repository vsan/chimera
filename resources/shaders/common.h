#ifndef CHIMERA_COMMON_H
#define CHIMERA_COMMON_H

#ifdef __cplusplus
#include "LiteMath.h"
using LiteMath::uint2;
using LiteMath::float2;
using LiteMath::float3;
using LiteMath::float4;
using LiteMath::float4x4;
using LiteMath::make_float2;
using LiteMath::make_float4;

typedef unsigned int uint;
typedef uint2        uvec2;
typedef float4       vec4;
typedef float3       vec3;
typedef float2       vec2;
typedef float4x4     mat4x4;
#endif

#define EMISSION_MTL 0
#define LAMBERT_MTL 1
#define MIRROR_MTL 2
#define GGX_MTL 3
#define BLEND_MTL 4

#define MAX_TEXTURES 256

//#define USE_MANY_HIT_SHADERS
//#define USE_CALLABLE_PIPELINE

const float PI = 3.14159265359f;

struct PushConst2MatricesID_T
{
  mat4x4 mProjView;
  mat4x4 mModel;
  uint instanceID;
};

struct PushConstMatrixViewID_T
{
  mat4x4 mProjView;
  vec3   viewPos;
  uint instanceID;
};

struct MaterialData_pbrMR
{
  vec4 baseColor;

  float metallic;
  float roughness;
  int baseColorTexId;
  int metallicRoughnessTexId;

  vec3 emissionColor;
  int emissionTexId;

  int normalTexId;
  int occlusionTexId;
  float alphaCutoff;
  int alphaMode;

};

struct UniformParams {
  vec4 camPos;
  vec4 camDir;
  vec4 camUp;
  vec4 camSide;
  vec4 camNearFarFov;
  mat4x4 viewInverse;
  mat4x4 projInverse;
  uint total_samples;
  uint accumulate_samples;
  uint update_rands_flag;
  uint vertexSize;
};

struct RayPayload {
  vec4 normalAndDist;
  vec4 baryMatID;
};

struct RayPayload_V2 {
  vec4 colorAndDist;
  vec4 ray_pos;
  vec4 ray_dir;
};

struct MaterialCallData
{
  vec3 pos;
  vec3 norm;
  vec2 rand;
  vec3 color;
  float roughness;
  vec4 ray_pos;
  vec4 ray_dir;
};


#endif//CHIMERA_COMMON_H



