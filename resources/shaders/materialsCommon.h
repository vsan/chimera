#ifndef CHIMERA_MATERIALSCOMMON_H
#define CHIMERA_MATERIALSCOMMON_H

#ifndef M_PI
#define M_PI          3.14159265358979323846f
#endif

#ifndef M_TWOPI
#define M_TWOPI       6.28318530717958647692f
#endif

#ifndef INV_PI
#define INV_PI        0.31830988618379067154f
#endif

#ifndef MAXFLOAT
#define MAXFLOAT 1e37f
#endif

#define GEPSILON      5e-6f
#define DEPSILON      1e-20f


float LinearToSrgb(float channel)
{
  if (channel <= 0.0031308f) {
    return 12.92f * channel;
  } else {
    return 1.055f * pow(channel, 1.0f / 2.4f) - 0.055f;
  }
}

float LinearToSrgb_v2(float channel)
{
  return channel / (channel + 1.0f);
}

vec3 LinearToSrgb(vec3 linear)
{
  return vec3(LinearToSrgb(linear.r), LinearToSrgb(linear.g), LinearToSrgb(linear.b));
}

vec3 LinearToSrgb_v2(vec3 linear)
{
  return vec3(LinearToSrgb_v2(linear.r), LinearToSrgb_v2(linear.g), LinearToSrgb_v2(linear.b));
}

void CoordinateSystem(vec3 v1, inout vec3 v2, inout vec3 v3)
{
  float invLen = 1.0f;

  if (abs(v1.x) > abs(v1.y))
  {
    invLen = 1.0f / sqrt(v1.x*v1.x + v1.z*v1.z);
    v2  = vec3((-1.0f) * v1.z * invLen, 0.0f, v1.x * invLen);
  }
  else
  {
    invLen = 1.0f / sqrt(v1.y * v1.y + v1.z * v1.z);
    v2  = vec3(0.0f, v1.z * invLen, (-1.0f) * v1.y * invLen);
  }

  v3 = cross(v1, v2);
}

vec3 MapSampleToCosineDistribution(float r1, float r2, vec3 direction, vec3 hit_norm, float power)
{
  if(power >= 1e6f)
    return direction;

  const float sin_phi = sin(M_TWOPI * r1);
  const float cos_phi = cos(M_TWOPI * r1);

  //sincos(2.0f*r1*3.141592654f, &sin_phi, &cos_phi);

  const float cos_theta = pow(1.0f - r2, 1.0f / (power + 1.0f));
  const float sin_theta = sqrt(1.0f - cos_theta*cos_theta);

  vec3 deviation;
  deviation.x = sin_theta*cos_phi;
  deviation.y = sin_theta*sin_phi;
  deviation.z = cos_theta;

  vec3 ny = direction, nx, nz;
  CoordinateSystem(ny, nx, nz);

  {
    vec3 temp = ny;
    ny = nz;
    nz = temp;
  }

  vec3 res = nx*deviation.x + ny*deviation.y + nz*deviation.z;

  float invSign = dot(direction, hit_norm) > 0.0f ? 1.0f : -1.0f;

  if (invSign*dot(res, hit_norm) < 0.0f) // reflected ray is below surface #CHECK_THIS
  {
    res = (-1.0f)*nx*deviation.x + ny*deviation.y - nz*deviation.z;
    //belowSurface = true;
  }

  return res;
}

vec3 GetPerpendicularVector(vec3 u)
{
  vec3 a = abs(u);
  uint xm = ((a.x - a.y) < 0 && (a.x - a.z) < 0) ? 1 : 0;
  uint ym = (a.y - a.z) < 0 ? (1 ^ xm) : 0;
  uint zm = 1 ^ (xm | ym);
  return cross(u, vec3(xm, ym, zm));
}

vec3 CosineWeightedHemisphereSample(vec2 random, vec3 normal)
{
  vec3 bitangent = GetPerpendicularVector(normal);
  vec3 tangent   = cross(bitangent, normal);
  float r        = sqrt(random.x);
  float phi      = 2.0f * 3.14159265f * random.y;

  return tangent * (r * cos(phi).x) + bitangent * (r * sin(phi)) + normal.xyz * sqrt(1 - random.x);
}

float epsilonOfPos(vec3 hitPos)
{
  return max(max(abs(hitPos.x), max(abs(hitPos.y), abs(hitPos.z))), 2.0f*GEPSILON)*GEPSILON;
}


vec3 OffsRayPos(const vec3 a_hitPos, const vec3 a_surfaceNorm, const vec3 a_sampleDir)
{
  const float signOfNormal2 = dot(a_sampleDir, a_surfaceNorm) < 0.0f ? -1.0f : 1.0f;
  const float offsetEps     = epsilonOfPos(a_hitPos);
  return a_hitPos + signOfNormal2 * offsetEps * a_surfaceNorm;
}

vec3 SphericalDirectionPBRT(const float sintheta, const float costheta, const float phi)
{
  return vec3(sintheta * cos(phi), sintheta * sin(phi), costheta);
}

float GGX_Distribution(const float cosThetaNH, const float alpha)
{
  const float alpha2  = alpha * alpha;
  const float NH_sqr  = clamp(cosThetaNH * cosThetaNH, 0.0f, 1.0f);
  const float den     = NH_sqr * alpha2 + (1.0f - NH_sqr);
  return alpha2 / max(float(M_PI) * den * den, 1e-6f);
}

float GGX_GeomShadMask(const float cosThetaN, const float alpha)
{
  const float cosTheta_sqr = clamp(cosThetaN*cosThetaN, 0.0f, 1.0f);
  const float tan2         = (1.0f - cosTheta_sqr) / max(cosTheta_sqr, 1e-6f);
  const float GP           = 2.0f / (1.0f + sqrt(1.0f + alpha * alpha * tan2));
  return GP;
}

vec2 hash2(vec2 p )
{
  const vec2 p1 = vec2(dot(p, vec2(123.4f, 748.6f)), dot(p, vec2(547.3f, 659.3f)));
  return fract(vec2(sin(p1.x)*5232.85324f, sin(p1.y)*5232.85324f));
}

//Based off of iq's described here: http://www.iquilezles.org/www/articles/voronoilin
//
float voronoi(vec2 p, float iTime)
{
  vec2 n = floor(p);
  vec2 f = fract(p);
  float md = 5.0f;
  vec2 m = vec2(0.0f, 0.0f);
  for (int i = -1;i<=1;i++) {
    for (int j = -1;j<=1;j++) {
      vec2 g = vec2(i, j);
      vec2 o = hash2(n+g);
      o.x = 0.5f+0.5f*sin(iTime+5.038f*o.x);
      o.y = 0.5f+0.5f*sin(iTime+5.038f*o.y);
      vec2 r = g + o - f;
      float d = dot(r, r);
      if (d<md) {
        md = d;
        m = n+g+o;
      }
    }
  }
  return md;
}

float ov(vec2 p, float iTime)
{
  float v = 0.0f;
  float a = 0.4f;
  for (int i = 0;i<3;i++) {
    v+= voronoi(p, iTime)*a;
    p *= 2.0f;
    a *= 0.5f;
  }
  return v;
}

vec4 BlueWhiteColor(const vec3 pos)
{
  const vec4 a  = vec4(0.20f, 0.4f, 1.0f, 1.0f);
  const vec4 b  = vec4(0.85f, 0.9f, 1.0f, 1.0f)*2.0f;

  return mix(a, b, smoothstep(0.0f, 0.35f, ov(vec2(pos.x, pos.y), pos.z)));
}

// https://www.shadertoy.com/view/4sfGzS
float hash(vec3 p)
{
  p  = fract( p*0.3183099f + vec3(0.1f,0.1f,0.1f) );
  p *= 17.0f;
  return fract( p.x*p.y*p.z*(p.x+p.y+p.z) );
}

float noise(vec3 x)
{
  vec3 p = floor(x);
  vec3 f = fract(x);
  f = f*f*(vec3(3.0f,3.0f,3.0f) - 2.0f*f);

  return mix(mix(mix( hash(p + vec3(0,0,0)),
                      hash(p + vec3(1,0,0)),f.x),
                 mix( hash(p + vec3(0,1,0)),
                      hash(p + vec3(1,1,0)),f.x),f.y),
             mix(mix( hash(p + vec3(0,0,1)),
                      hash(p + vec3(1,0,1)),f.x),
                 mix( hash(p + vec3(0,1,1)),
                      hash(p + vec3(1,1,1)),f.x),f.y),f.z);
}


vec3 WhiteNoise(const vec3 pos)
{
  const mat3 m = mat3(0.00f,  0.80f,  0.60f,
                      -0.80f,  0.36f, -0.48f,
                      -0.60f, -0.48f,  0.64f);

  float f = 0.0f;

  vec3 q = 10.0f * pos;
  f  = 0.5000f*noise( q ); q = m * (q*2.01f);
  f += 0.2500f*noise( q ); q = m * (q*2.02f);
  f += 0.1250f*noise( q ); q = m * (q*2.03f);
  f += 0.0625f*noise( q ); q = m * (q*2.01f);

  return vec3(f, f, f);
}

//////////////////////////////////////////////////////////////////////////
vec3 LambertNext(vec3 pos, vec3 N, vec2 rand, vec3 color, inout vec4 rayPos, inout vec4 rayDir)
{
  //CosineWeightedHemisphereSample(rand, N);
  const vec3 newDir    = MapSampleToCosineDistribution(rand.x, rand.y, N, N, 1.0f);
  const float cosTheta = dot(newDir, N);

  const float pdfVal = cosTheta * INV_PI;
  const vec3 brdfVal = (cosTheta > 1e-5f) ? color * INV_PI : vec3(0.0f);
  const vec3 bxdfVal = brdfVal * (1.0f / max(pdfVal, 1e-10f));

  rayPos = vec4(OffsRayPos(pos, N, newDir), 0.0f);
  rayDir = vec4(newDir, MAXFLOAT);

  return cosTheta * bxdfVal;
}

vec3 PerfectMirrorNext(vec3 pos, vec3 N, vec3 color, inout vec4 rayPos, inout vec4 rayDir)
{
  vec3 newDir = reflect(rayDir.xyz, N);
  if (dot(rayDir.xyz,  N) > 0.0f)
    newDir = rayDir.xyz;

  rayPos = vec4(OffsRayPos(pos, N, newDir), 0.0f);
  rayDir = vec4(newDir, MAXFLOAT);
  return color;
}

vec3 GGXGlossy(vec3 pos, vec3 N, vec2 rand, vec3 color, float roughness, inout vec4 rayPos, inout vec4 rayDir)
{
  const float  roughSqr   = roughness * roughness;
  vec3 nx, ny, nz       = N;
  CoordinateSystem(nz, nx, ny);

  // to PBRT coordinate system
  const vec3 wo = vec3(-dot(rayDir.xyz, nx), -dot(rayDir.xyz, ny), -dot(rayDir.xyz, nz));  // wo (output) = v = ray_dir

  // Compute sampled half-angle vector wh
  const float phi       = rand.x * M_TWOPI;
  const float cosTheta  = clamp(sqrt((1.0f - rand.y) / (1.0f + roughSqr * roughSqr * rand.y - rand.y)), 0.0f, 1.0f);
  const float sinTheta  = sqrt(1.0f - cosTheta * cosTheta);
  const vec3 wh         = SphericalDirectionPBRT(sinTheta, cosTheta, phi);
  const vec3 wi         = (2.0f * dot(wo, wh) * wh) - wo;                  // Compute incident direction by reflecting about wh. wi (input) = light
  const vec3 newDir     = normalize(wi.x*nx + wi.y*ny + wi.z*nz);          // back to normal coordinate system

  float Pss       = 1.0f;  // Pass single-scattering
  const vec3 V    = rayDir.xyz * (-1.0f);
  const vec3 L    = newDir;
  const float dotNV = dot(N, V);
  const float dotNL = dot(N, L);

  float outPdf = 1.0f;
  if (dotNV < 1e-6f || dotNL < 1e-6f)
  {
    Pss    = 0.0f;
    outPdf = 1.0f;
  }
  else
  {
    const vec3 H    = normalize(V + L);  // half vector.
    const float dotNV = dot(N, V);
    const float dotNH = dot(N, H);
    const float dotHV = dot(H, V);

    // Fresnel is not needed here, because it is used for the blend with diffusion.
    const float D = GGX_Distribution(dotNH, roughSqr);
    const float G = GGX_GeomShadMask(dotNV, roughSqr) * GGX_GeomShadMask(dotNL, roughSqr);

    Pss    = D * G / max(4.0f * dotNV * dotNL, 1e-6f);
    outPdf = D * dotNH / max(4.0f * dotHV, 1e-6f);
  }

  rayPos = vec4(OffsRayPos(pos, N, newDir), 0.0f);
  rayDir = vec4(newDir, MAXFLOAT);
  return cosTheta * color * Pss * (1.0f/max(outPdf, 1e-5f));
}


vec3 BlendLambertGGX(vec3 in_pos, vec3 N, vec4 rand, vec3 color, float roughness, inout vec4 rayPos, inout vec4 rayDir)
{
  const vec3 pos   = in_pos * 8.0f;
  const float select = ov(vec2(pos.z, pos.x), pos.y);

  vec3 colorA = WhiteNoise(in_pos * 5.0f);
  vec3 colorC = vec3(0.8f, 0.1f, 0.8f);

  colorC = clamp(colorC, 0.0f, 1.0f);
  colorA = clamp(colorA, 0.0f, 1.0f);

  vec3 return_color = vec3(0.0f);
  if(rand.z <= select)
  {
    return_color = GGXGlossy(in_pos, N, rand.xy, color, 0.25f, rayPos, rayDir);
  }
  else
  {
    return_color = LambertNext(in_pos, N, rand.xy, color, rayPos, rayDir);
  }

  return return_color;
}

bool BlendSelect(vec3 in_pos, float rand)
{
  const vec3 pos   = in_pos * 8.0f;
  const float select = ov(vec2(pos.z, pos.x), pos.y);

  vec3 colorA = WhiteNoise(in_pos * 5.0f);
  vec3 colorC = vec3(0.8f, 0.1f, 0.8f);

  colorC = clamp(colorC, 0.0f, 1.0f);
  colorA = clamp(colorA, 0.0f, 1.0f);

  vec3 return_color = vec3(0.0f);

  return rand <= select;
}

struct Vertex
{
  vec3 pos;
  vec3 normal;
  vec2 uv;
};

vec3 DecodeNormal(uint a_data)
{
  const uint a_enc_x = (a_data  & 0x0000FFFFu);
  const uint a_enc_y = ((a_data & 0xFFFF0000u) >> 16);
  const float sign   = (a_enc_x & 0x0001u) != 0 ? -1.0f : 1.0f;

  const int usX = int(a_enc_x & 0x0000FFFEu);
  const int usY = int(a_enc_y & 0x0000FFFFu);

  const int sX  = (usX <= 32767) ? usX : usX - 65536;
  const int sY  = (usY <= 32767) ? usY : usY - 65536;

  const float x = sX*(1.0f / 32767.0f);
  const float y = sY*(1.0f / 32767.0f);
  const float z = sign*sqrt(max(1.0f - x*x - y*y, 0.0f));

  return vec3(x, y, z);
}

#endif//CHIMERA_MATERIALSCOMMON_H
