#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) out vec4 out_fragColor;

layout (binding = 0) uniform sampler2D sampler_posZ;
layout (binding = 1) uniform sampler2D sampler_normal;
layout (binding = 2) uniform sampler2D sampler_albedo;

layout (location = 0) in vec2 inUV;

void main()
{
    vec3 lightDir1 = vec3(1.0f, 1.0f, 1.0f);
    vec3 lightDir2 = vec3(-1.0f, 1.0f, 1.0f);
    vec3 lightDir3 = vec3(0.0f, 1.0f, 1.0f);

    vec3 lightColor1 = vec3(0.64f, 1.0f, 1.0f);
    vec3 lightColor2 = vec3(1.0f, 0.56f, 0.001f);
    vec3 lightColor3 = vec3(1.0f, 1.0f, 1.0f);

//    vec3 lightColor1 = vec3(1.0f, 0.0f, 0.0f);
//    vec3 lightColor2 = vec3(0.0f, 1.0f, 0.0f);
//    vec3 lightColor3 = vec3(1.0f, 1.0f, 1.0f);

    vec3 fragPos = texture(sampler_posZ, inUV).rgb;
    vec3 N = normalize(texture(sampler_normal, inUV).rgb * 2.0 - 1.0);
    vec4 albedo = texture(sampler_albedo, inUV);
//
    vec4 color1 = vec4(abs(dot(N, lightDir1)) * lightColor1, 1.0f);
    vec4 color2 = vec4(abs(dot(N, lightDir2)) * lightColor2, 1.0f);
    vec4 color3 = vec4(abs(dot(N, lightDir3)) * lightColor3, 1.0f);
    vec4 color_l = 0.45f * color1 + 0.45f * color2 + 0.1f * color3;

    vec4 base_color = vec4(0.8f, 0.8f, 0.8f, 1.0f);

    out_fragColor = base_color * color_l;
//    out_fragColor = vec4(fragPos, 1.0);
}