import os
import subprocess
import pathlib

if __name__ == '__main__':
    glslang_cmd = "glslangValidator"

    shader_list = ["simple.vert", "simple.frag", "wireframe.vert", "wireframe.frag",
                   "gbuf.vert", "gbuf.frag", "quad.vert", "quad.frag", "pbrMetallicRoughnessForward.frag",
                   "pbrMetallicRoughnessForwardSpec.frag"]

    for shader in shader_list:
        subprocess.run([glslang_cmd, "-V", shader, "-o", "{}.spv".format(shader)])

    # #################################

    rt_shader_list = ["raygen_basic.rgen", "closesthit_basic.rchit", "miss_basic.rmiss",
                      "minimal.rgen", "minimal.rchit", "minimal.rmiss", "rayquery.frag"]

    for shader in rt_shader_list:
        subprocess.run([glslang_cmd,  "--target-env", "vulkan1.2", "-V", shader, "-o", "{}.spv".format(shader)])

    # #################################

    pt_folder = "path_tracing"
    pt_shader_list = ["raygen_pt.rgen", "closesthit_pt.rchit", "miss_pt.rmiss",
                      "raygen_callable.rgen", "emission.rcall", "ggx.rcall", "lambert.rcall", "mirror.rcall",
                      "raygen_many_hit_shaders.rgen", "hit_lambert.rchit", "hit_emission.rchit", "hit_mirror.rchit",
                      "hit_ggx.rchit", "hit_blend.rchit"]

    for shader in pt_shader_list:
        subprocess.run([glslang_cmd,  "--target-env", "vulkan1.2", "-V", "{}/{}".format(pt_folder, shader),
                        "-o", "{}/{}.spv".format(pt_folder, shader)])

    # #################################

    comp_shader_list = ["raytrace.comp"]

    for shader in comp_shader_list:
        subprocess.run([glslang_cmd,  "--target-env", "vulkan1.2", "-V", shader, "-o", "{}.spv".format(shader)])
