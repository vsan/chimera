#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 out_fragColor;

layout (location = 0 ) in VS_OUT
{
    vec3 wPos;
    vec3 wNorm;
    vec3 wTangent;
    vec2 texCoord;

} surf;

//layout (binding = 0) uniform sampler2D  diffColor;


void main()
{
    vec3 lightDir1 = vec3(1.0f, 1.0f, 1.0f);
    vec3 lightDir2 = vec3(-1.0f, 1.0f, 1.0f);
    vec3 lightDir3 = vec3(0.0f, 1.0f, 1.0f);

    vec3 lightColor1 = vec3(0.64f, 1.0f, 1.0f);
    vec3 lightColor2 = vec3(1.0f, 0.56f, 0.001f);
    vec3 lightColor3 = vec3(1.0f, 1.0f, 1.0f);

    //    vec3 lightColor1 = vec3(1.0f, 0.0f, 0.0f);
    //    vec3 lightColor2 = vec3(0.0f, 1.0f, 0.0f);
    //    vec3 lightColor3 = vec3(1.0f, 1.0f, 1.0f);

    vec3 N = surf.wNorm; //dot(surf.wNorm, (params.wCamPos.xyz - surf.wPos)) > 0 ? surf.wNorm : -surf.wNorm;

    vec4 color1 = vec4(abs(dot(N, lightDir1)) * lightColor1, 1.0f);
    vec4 color2 = vec4(abs(dot(N, lightDir2)) * lightColor2, 1.0f);
    vec4 color3 = vec4(abs(dot(N, lightDir3)) * lightColor3, 1.0f);
    vec4 color_l = 0.45f * color1 + 0.45f * color2 + 0.1f * color3;

    vec4 base_color = vec4(0.8f, 0.8f, 0.8f, 1.0f);

    out_fragColor = base_color * color_l;
}
