#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) out vec4 outPosZ;
layout (location = 1) out vec4 outNormal;
layout (location = 2) out vec4 outAlbedo;
//layout (location = 3) out vec4 out_fragColor;

layout (location = 0 ) in VS_OUT
{
    vec3 wPos;
    vec3 wNorm;
    vec3 wTangent;
    vec2 texCoord;

} surf;


//float linearizeDepth(float depth)
//{
//    float z = depth * 2.0f - 1.0f;
//    return (2.0f * nearPlane * farPlane) / (farPlane + nearPlane - z * (farPlane - nearPlane));
//}

void main()
{
    outPosZ = vec4(surf.wPos, gl_FragCoord.z);
    outNormal = vec4(normalize(surf.wNorm) * 0.5 + 0.5, 1.0);
//
//    //@TODO: sample diffuse texture
    outAlbedo = vec4(surf.texCoord, 0.0, 1.0);
//    outPosZ = vec4(0.0f, 0.0f, 1.0f, 1.0f);
//    outNormal = vec4(0.0f, 1.0f, 0.0f, 1.0f);
//    outAlbedo = vec4(1.0f, 0.0f, 0.0f, 1.0f);
//    out_fragColor = vec4(0.0);
}
